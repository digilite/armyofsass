<?php
/**
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package sass
 */

// Default time zone
date_default_timezone_set('America/Toronto');

if ( ! function_exists( 'sass_setup' ) ) :

function sass_setup() {
  load_theme_textdomain( 'sass', get_template_directory() . '/languages' );

  /* THEME SUPPORTS */
  add_theme_support( 'automatic-feed-links' );
  add_theme_support( 'title-tag' );
  add_theme_support( 'post-thumbnails' );
  add_theme_support( 'customize-selective-refresh-widgets' );
  add_theme_support( 'html5', array(
    'search-form',
    'comment-form',
    'comment-list',
    'gallery',
    'caption',
  ) );

  add_image_size( 'team-thumb', 640, 540, true );
  add_image_size( 'team-full', 300, 900, false );
  add_image_size( 'post-thumb', 400, 400, true );

  /* MEMUS */
  register_nav_menus( array(
    'menu-primary' => esc_html__( 'Primary', 'sass' ),
    'menu-location' => esc_html__( 'Location', 'sass' ),
    'menu-social' => esc_html__( 'Social', 'sass' )
  ) );
}
endif;
add_action( 'after_setup_theme', 'sass_setup' );


/**
 * Register widgets
 */
function sass_widgets_init() {
  register_sidebar( array(
    'name'          => esc_html__( 'Blog sidebar', 'sass' ),
    'id'            => 'sidebar-blog',
    'description'   => esc_html__( 'Add widgets here.', 'sass' ),
    'before_widget' => '<section id="%1$s" class="widget %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h2 class="widget-title">',
    'after_title'   => '</h2>',
  ) );
}
add_action( 'widgets_init', 'sass_widgets_init' );

/**
 * SCRIPTS & STYLES
 */
define( 'THEME_VERSION', '2018.06.10.01' );
//define( 'THEME_VERSION', time() );

function sass_scripts() {
  //wp_enqueue_style( 'montserrat', 'https://fonts.googleapis.com/css?family=Montserrat:200,400,600', array() );
  wp_enqueue_style( 'style', get_template_directory_uri() . '/css/site.min.css', array(), THEME_VERSION );

  if ( !is_admin() ) {
    wp_deregister_script('jquery');
    wp_register_script('jquery', get_template_directory_uri() . '/js/vendor/jquery.js', array(), '2.2.4');
    wp_enqueue_script('jquery');
  }

  wp_enqueue_script( 'foundation', get_template_directory_uri() . '/js/vendor/foundation.min.js', array(), '6.3.1', true );
  wp_enqueue_script( 'slick', get_template_directory_uri() . '/js/vendor/slick.min.js', array(), '1.16.0', true);
  wp_enqueue_script( 'scrollto', get_template_directory_uri() . '/js/vendor/scrollTo.min.js', array(), '2.1.1', true);
  wp_enqueue_script( 'localscroll', get_template_directory_uri() . '/js/vendor/localScroll.min.js', array(), '1.4.0', true);


  // wp_enqueue_script( 'form-populate-items',
  //  get_template_directory_uri() . '/js/form-populate-items.js',
  //  array(),
  //   '1.0.0',
  //    true);



  // wp_enqueue_script(
  //   'form-populate-items',
  //    get_template_directory_uri() . '/js/form-populate-items.js',
  //     array(),
  //      '1.0.0',
  //       true);
  if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
    wp_enqueue_script( 'comment-reply' );
  }

  if ( is_author() ) {

    wp_enqueue_script( 'isotope', get_template_directory_uri() . '/js/vendor/isotope.min.js', array(), '3.0.4', true );
    wp_enqueue_script( 'ajax-isotope', get_template_directory_uri() . '/js/isotope.min.js', array(), THEME_VERSION, true);
    global $wp_query;
    wp_localize_script( 'ajax-isotope', 'ajaxisotope', array(
      'ajaxurl' => admin_url( 'admin-ajax.php' ),
      'query_vars' => json_encode( $wp_query->query )
    ));
  }

  wp_enqueue_script( 'experience', get_template_directory_uri() . '/js/site.min.js', array(), THEME_VERSION, true);

}
add_action( 'wp_enqueue_scripts', 'sass_scripts' );


function wpb_adding_scripts() {
wp_register_script('form-populate-items',
 get_template_directory_uri() . '/js/form-populate-items.js',
  array(),'1.1', true);
wp_enqueue_script('form-populate-items');
}

add_action( 'wp_enqueue_scripts', 'wpb_adding_scripts' );



remove_action('wp_head', 'wp_resource_hints', 2);
/**
 * Load Custom Post Types
 */
require get_template_directory() . '/inc/custom-post-types.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
* Disable WP default emoji's to improve performance
*/
function disable_emojis() {
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );
remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
add_filter( 'wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2 );
}
add_action( 'init', 'disable_emojis' );

/**
* Filter function used to remove the tinymce emoji plugin.
*
* @param array $plugins
* @return array Difference betwen the two arrays
*/
function disable_emojis_tinymce( $plugins ) {
if ( is_array( $plugins ) ) {
return array_diff( $plugins, array( 'wpemoji' ) );
} else {
return array();
}
}

/**
* Remove emoji CDN hostname from DNS prefetching hints.
*
* @param array $urls URLs to print for resource hints.
* @param string $relation_type The relation type the URLs are printed for.
* @return array Difference betwen the two arrays.
*/
function disable_emojis_remove_dns_prefetch( $urls, $relation_type ) {
if ( 'dns-prefetch' == $relation_type ) {
/** This filter is documented in wp-includes/formatting.php */
$emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );

$urls = array_diff( $urls, array( $emoji_svg_url ) );
}

return $urls;
}


/**
 * Rewrite author URL
 */
add_filter('author_rewrite_rules', 'no_author_base_rewrite_rules');
function no_author_base_rewrite_rules($author_rewrite) {
  global $wpdb;
  $author_rewrite = array();
  $authors = $wpdb->get_results("SELECT user_nicename AS nicename from $wpdb->users");
  foreach($authors as $author) {
      $author_rewrite["({$author->nicename})/page/?([0-9]+)/?$"] = 'index.php?author_name=$matches[1]&paged=$matches[2]';
      $author_rewrite["({$author->nicename})/?$"] = 'index.php?author_name=$matches[1]';
  }
  return $author_rewrite;
}
add_filter('author_link', 'no_author_base', 1000, 2);
function no_author_base($link, $author_id) {
  $link_base = trailingslashit(get_option('home'));
  $link = preg_replace("|^{$link_base}author/|", '', $link);
  return $link_base . $link;
}

/**
 * Sets the user's display name to their login
 */
add_action ('admin_head','make_display_name_f_name_last_name');
function make_display_name_f_name_last_name(){
  $users = get_users(array('fields'=>'all'));

  foreach($users as $user){
    $user = get_userdata($user->ID);

    $display_name = $user->nicename;

    if($display_name!=' ') wp_update_user( array ('ID' => $user->ID, 'display_name' => $display_name) );
        else wp_update_user( array ('ID' => $user->ID, 'display_name' => $user->display_login) );

    if($user->display_name == '')
        wp_update_user( array ('ID' => $user->ID, 'display_name' => $user->display_login) );
  }
}


/*
 * Hide Admin bar
 */

show_admin_bar(false);


/**
 * Custom admin CSS
 */

add_action('admin_head', 'custom_admin_css');
function custom_admin_css() {
  echo '<style>
    /* Update admin menu colours */
    #adminmenu .wp-has-current-submenu .wp-submenu .wp-submenu-head, #adminmenu .wp-menu-arrow, #adminmenu .wp-menu-arrow div, #adminmenu li.current a.menu-top, #adminmenu li.wp-has-current-submenu a.wp-has-current-submenu, .folded #adminmenu li.current.menu-top, .folded #adminmenu li.wp-has-current-submenu{background-color: #F1229F;}
    a, .wrap .add-new-h2, .wrap .add-new-h2:active, .wrap .page-title-action, .wrap .page-title-action:active { color: #0387c4; }
    a:active, a:hover {background-color: rgba(255,255,255,0); color: #F1229F; }
    #adminmenu li.wp-menu-separator {height:0;margin:0;border-top:2px solid #000;}
    #adminmenu div.separator {height:0;}
    #adminmenu .wp-has-current-submenu .wp-submenu, #adminmenu .wp-has-current-submenu .wp-submenu.sub-open, #adminmenu .wp-has-current-submenu.opensub .wp-submenu, #adminmenu a.wp-has-current-submenu:focus+.wp-submenu, .no-js li.wp-has-current-submenu:hover .wp-submenu{background-color:#133344;}
    /* Hide unneccessary profile options */
    form#your-profile .yoast-settings, form#your-profile .user-profile-picture, form#your-profile h2, form#your-profile table:first-of-type, form#your-profile .user-nickname-wrap, form#your-profile .user-display-name-wrap, form#your-profile .user-url-wrap, form#your-profile .user-description-wrap { display: none !important; }
  </style>';

  // If current user is a Location User
  $current_user = wp_get_current_user();
  $licenseeOrNot = array_keys($current_user->caps);
  if ( $licenseeOrNot[0] == "location_user" ) {
    echo '<style>

    .subsubsub .all, .subsubsub .publish, .subsubsub .draft, .subsubsub .trash, .post-type-registration #misc-publishing-actions, .author-other, .term-description-wrap, .term-parent-wrap, .wpseo-taxonomy-metabox-postbox, .acf-field-58f7bbd100ff8, .acf-field-592852d452650, .acf-field-592853c9ea1ad, #wpseo-filter, #add_gform, .media-menu .media-menu-item:last-of-type, .media-menu .media-menu-item:nth-of-type(2), .media-menu .media-menu-item:nth-of-type(3), .media-menu .media-menu-item:nth-of-type(4), .misc-pub-visibility, .yoast, .post-type-registration #minor-publishing-actions, .row-actions .view, #edit-slug-box, .post-type-registration .acf-field[data-name="gfentryid"], #position-56, #popular-position-56, select#wpseo-readability-filter { display: none !important; }

    .subsubsub .mine a {
      pointer-events: auto !important;
      cursor: pointer !important;
    }

    .acf-relationship .list { height: 55px;}

    .mine a {pointer-events: none;cursor: default;}

    #taxonomy-level #level-all { max-height: 100%; }

    .column-session, .column-taxonomy-place-type, .column-photo, .column-image { width: 150px;}
    .column-contact { width: 200px; }
    .column-banner { width: 300px; }
    .column-class {width: 350px; }

    .post-type-registration .acf-input-wrap {line-height:1rem;}

    .post-type-registration .acf-postbox.seamless > .acf-fields > .acf-field {padding:10px 0 10px 10px;}

    .post-type-registration .acf-field .acf-label {margin:0;}

    .post-type-registration:not(.post-new-php) input#title, .post-type-registration:not(.post-new-php) input[type="email"], .post-type-registration:not(.post-new-php) input#acf-field_58ef9c17295d9, .post-type-registration:not(.post-new-php) input#acf-field_58ef9cbf5f526, .post-type-registration:not(.post-new-php) input#acf-field_58ef9e5c39a80, .post-type-registration:not(.post-new-php) input#acf-field_58efa06b11ad7, .post-type-registration:not(.post-new-php) input#acf-field_58efa27988a15, .post-type-registration:not(.post-new-php) input#acf-field_58efa27988a07, .post-type-registration:not(.post-new-php) input#acf-field_58fe14803fd57, .post-type-registration:not(.post-new-php) input#acf-field_58fe14a93fd58, .post-type-registration:not(.post-new-php) input#acf-field_58fe16b5d68c8, .post-type-registration:not(.post-new-php) textarea#acf-field_58ef9cbf5f526, .post-type-registration input#acf-field_5902594e8598e {background-color:#f1f1f1 !important;color: #000;padding:0 !important;border:none;box-shadow:none;opacity: .5; pointer-events: none;line-height:1rem; }

    .post-type-registration:not(.post-new-php) input#title {margin-top:3px !important; opacity: 0.8;border-top: 2px solid #000;border-bottom: 1px dotted #000;}


    </style>';
  }
}

/**
 *  Hide unneccessary options from Profile edit
 */
function hide_instant_messaging( $contactmethods ) {
  unset($contactmethods['aim']);
  unset($contactmethods['yim']);
  unset($contactmethods['jabber']);
  unset($contactmethods['googleplus']);
  unset($contactmethods['twitter']);
  unset($contactmethods['facebook']);
  return $contactmethods;
}
add_filter('user_contactmethods','hide_instant_messaging',10,1);

/**
 *  Remove Yoast SEO meta box
 */
add_action( 'add_meta_boxes', 'remove_yoast_meta_boxes', 10 );
function remove_yoast_meta_boxes(){
  remove_meta_box( 'wpseo_meta', 'team', 'normal' );
  remove_meta_box( 'wpseo_meta', 'place', 'normal' );
  remove_meta_box( 'wpseo_meta', 'class', 'normal' );
  remove_meta_box( 'wpseo_meta', 'show', 'normal' );
  remove_meta_box( 'wpseo_meta', 'registration', 'normal' );
  remove_meta_box( 'wpseo_meta', 'session', 'normal' );
  remove_meta_box( 'wpseo_meta', 'spotlight', 'normal' );
}

/**
 * Hide other users' media files in Media Library
 */

add_action('pre_get_posts','users_own_attachments');
function users_own_attachments( $wp_query ) {
  global $current_user, $pagenow;

  $is_attachment_request = ($wp_query->get('post_type')=='attachment');

  if( !$is_attachment_request )
    return;

  if( !is_a( $current_user, 'WP_User') )
    return;

  if( !in_array( $pagenow, array( 'upload.php', 'admin-ajax.php' ) ) )
    return;

  if( !current_user_can('update_core') )
    $wp_query->set('author', $current_user->ID );
  return;
}

/**
 * Enable ACF Options page
 */
if( function_exists('acf_add_options_page') ) {
  if (current_user_can('update_core')) {
    acf_add_options_page();
  }
}

/**
 * Change Gravity Forms' default error message
 */
add_filter( 'gform_validation_message', 'change_message', 10, 2 );
function change_message( $message, $form ) {
  return '<label class="validation_error">Please fill out the required fields to continue</label>';
}

/*
 * Customize Location admin menu
 */
function custom_location_menu($menu_ord) {
  if (!$menu_ord) return true;

  // If current user is a Location User
  $current_user = wp_get_current_user();
  $licenseeOrNot = array_keys($current_user->caps);
  if ( $licenseeOrNot[0] == "location_user" ) {

    remove_menu_page( 'index.php' );
    remove_menu_page( 'upload.php' );
    remove_menu_page( 'tools.php' );
    remove_menu_page( 'edit-comments.php' );
    remove_menu_page( 'edit.php' );

    return array(
      'profile.php', // Account
      'separator1', // Second separator
      'edit.php?post_type=team', // Team
      'edit.php?post_type=place', // Places
      'edit.php?post_type=session', // Sessions
      'edit.php?post_type=class', // Classes
      'edit.php?post_type=show', // Shows
      'edit.php?post_type=spotlight', // Spotlight
      'separator2', // Third separator
      'edit.php?post_type=registration', // Places
      'separator-last', // Last separator
    );
  } else {
    remove_menu_page( 'link-manager.php' );
  }
}
add_filter('custom_menu_order', 'custom_location_menu');
add_filter('menu_order', 'custom_location_menu');

/*
 *
 */
function hide_update_notice_to_all_but_admin_users(){
  if (!current_user_can('update_core')) {
      remove_action( 'admin_notices', 'update_nag', 3 );
  }
}
add_action( 'admin_head', 'hide_update_notice_to_all_but_admin_users', 1 );


/*
 * Add Google Map API Key to ACF
 */
add_filter('acf/settings/google_api_key', function ($value) {
  return 'AIzaSyBRJQZmfIocgU3xnfbgmmlFHaM5l0Okm5o';
});

/*
 * Limit ACF Relationship fields to load user's own posts only
 */
add_filter('acf/fields/relationship/query', 'relationship_only_own_posts', 10, 3);
function relationship_only_own_posts($args, $field, $post_id) {
  if (current_user_can('update_core')) {
    return $args;
  }
  $currentAuth = get_current_user_id();
  $args['author'] = $currentAuth;
  return $args;
}

/*
 * Map ACF to Location profile
 */
add_filter('acf/location/rule_types', 'acf_location_rules_types');
function acf_location_rules_types( $choices ){
  $choices['Basic']['plocation'] = 'Location Profile';
  return $choices;
}

add_filter('acf/location/rule_values/plocation', 'acf_location_rules_values_plocation');
function acf_location_rules_values_plocation( $choices ){
  $choices[0] = "profile.php";
  $choices[1] = "user-new.php";
  $choices[2] = "user-edit.php";
  return $choices;
}

add_filter('acf/location/rule_match/plocation', 'acf_location_rules_match_plocation', 10, 3);
function acf_location_rules_match_plocation( $match, $rule, $options ){
  $pageArrays = [];
  $pageArrays[0] = "profile.php";
  $pageArrays[1] = "user-new.php";
  $pageArrays[2] = "user-edit.php";

  $wpProfilePage = basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']);
  $selected_page = $rule['value'];
  $current_user = wp_get_current_user();
  $adminOrNot = array_keys($current_user->caps);

  if($rule['operator'] == "=="){
    $match = ( $wpProfilePage == $pageArrays[$selected_page] );
  } elseif($rule['operator'] == "!="){
    $match = ( $wpProfilePage != $pageArrays[$selected_page] );
  }

  if ($adminOrNot[0] == "administrator" && $wpProfilePage == "profile.php") {
    return 0;
  } else {
    return $match;
  }
}

if( function_exists('acf_add_local_field_group') ):
  acf_add_local_field_group(array (
    'key' => 'group_58efec12749f3',
    'title' => 'Location Options',
    'fields' => array (
      array (
        'key' => 'field_58efec5301e82',
        'label' => 'Facebook URL',
        'name' => 'facebook_url',
        'type' => 'url',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => 0,
        'wrapper' => array (
          'width' => '40',
          'class' => '',
          'id' => '',
        ),
        'default_value' => '',
        'placeholder' => '',
      ),
      array (
        'key' => 'field_58efec6601e83',
        'label' => 'Instagram URL',
        'name' => 'instagram_url',
        'type' => 'url',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => 0,
        'wrapper' => array (
          'width' => '40',
          'class' => '',
          'id' => '',
        ),
        'default_value' => '',
        'placeholder' => '',
      ),
      array (
        'key' => 'field_58efec9801e84',
        'label' => 'Enable online registration',
        'name' => 'payment_methods',
        'type' => 'checkbox',
        'instructions' => '',
        'required' => 1,
        'conditional_logic' => 0,
        'wrapper' => array (
          'width' => '',
          'class' => '',
          'id' => '',
        ),
        'choices' => array (
          'Paypal/credit card' => 'Paypal/credit card',
          'Email money transfer' => 'Email money transfer',
          'Cash' => 'Cash',
          'External' => 'External link (if chosen, other options will be disabled)',
        ),
        'allow_custom' => 0,
        'save_custom' => 0,
        'default_value' => array (
        ),
        'layout' => 'vertical',
        'toggle' => 0,
        'return_format' => 'value',
      ),
      array (
        'key' => 'field_58efed7e01e85',
        'label' => 'Paypal account email',
        'name' => 'paypal_email',
        'type' => 'email',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => array (
          array (
            array (
              'field' => 'field_58efec9801e84',
              'operator' => '==',
              'value' => 'Paypal/credit card',
            ),
          ),
        ),
        'wrapper' => array (
          'width' => '',
          'class' => '',
          'id' => '',
        ),
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
      ),
      array (
        'key' => 'field_58efedb001e86',
        'label' => 'External link',
        'name' => 'external_link',
        'type' => 'url',
        'instructions' => '',
        'required' => 1,
        'conditional_logic' => array (
          array (
            array (
              'field' => 'field_58efec9801e84',
              'operator' => '==',
              'value' => 'External',
            ),
          ),
        ),
        'wrapper' => array (
          'width' => '',
          'class' => '',
          'id' => '',
        ),
        'default_value' => '',
        'placeholder' => '',
      ),
      array (
        'key' => 'field_58efedd101e87',
        'label' => 'Tax percentage',
        'name' => 'tax_percentage',
        'type' => 'radio',
        'instructions' => '',
        'required' => 1,
        'conditional_logic' => 0,
        'wrapper' => array (
          'width' => '',
          'class' => '',
          'id' => '',
        ),
        'choices' => array (
          0 => 'No Tax (Canada or US)',
          '0.0416' => 'US (4.16%)',
          '0.05' => 'Canada GST (5%)',
          '0.11' => 'Canada GST + PST (11%)',
          '0.12' => 'Canada GST + PST (12%)',
          '0.13' => 'Canada HST / GST + PST (13%)',
          '0.15' => 'Canada HST (15%)',
        ),
        'allow_null' => 0,
        'other_choice' => 0,
        'save_other_choice' => 0,
        'default_value' => '',
        'layout' => 'vertical',
        'return_format' => 'value',
      ),
    ),
    'location' => array (
      array (
        array (
          'param' => 'current_user_role',
          'operator' => '==',
          'value' => 'location_user',
        ),
        array (
          'param' => 'user_form',
          'operator' => '==',
          'value' => 'edit',
        ),
      ),
    ),
    'menu_order' => 0,
    'position' => 'side',
    'style' => 'seamless',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => 1,
    'description' => '',
  ));
endif;

/*
 * Bidirectional relationship
 */
function bidirectional_acf_update_value( $value, $post_id, $field  ) {

  // vars
  $field_name = $field['name'];
  $field_key = $field['key'];
  $global_name = 'is_updating_' . $field_name;


  // bail early if this filter was triggered from the update_field() function called within the loop below
  // - this prevents an inifinte loop
  if( !empty($GLOBALS[ $global_name ]) ) return $value;


  // set global variable to avoid inifite loop
  // - could also remove_filter() then add_filter() again, but this is simpler
  $GLOBALS[ $global_name ] = 1;


  // loop over selected posts and add this $post_id
  if( is_array($value) ) {

    foreach( $value as $post_id2 ) {

      // load existing related posts
      $value2 = get_field($field_name, $post_id2, false);


      // allow for selected posts to not contain a value
      if( empty($value2) ) {

        $value2 = array();

      }


      // bail early if the current $post_id is already found in selected post's $value2
      if( in_array($post_id, $value2) ) continue;


      // append the current $post_id to the selected post's 'related_posts' value
      $value2[] = $post_id;


      // update the selected post's value (use field's key for performance)
      update_field($field_key, $value2, $post_id2);

    }

  }


  // find posts which have been removed
  $old_value = get_field($field_name, $post_id, false);

  if( is_array($old_value) ) {

    foreach( $old_value as $post_id2 ) {

      // bail early if this value has not been removed
      if( is_array($value) && in_array($post_id2, $value) ) continue;


      // load existing related posts
      $value2 = get_field($field_name, $post_id2, false);


      // bail early if no value
      if( empty($value2) ) continue;


      // find the position of $post_id within $value2 so we can remove it
      $pos = array_search($post_id, $value2);


      // remove
      unset( $value2[ $pos] );


      // update the un-selected post's value (use field's key for performance)
      update_field($field_key, $value2, $post_id2);

    }

  }


  // reset global varibale to allow this filter to function as per normal
  $GLOBALS[ $global_name ] = 0;


  // return
    return $value;

}
add_filter('acf/update_value/name=session', 'bidirectional_acf_update_value', 10, 3);

/*
 * Dynamically switch currency in Registration form
 */

add_filter( 'gform_currency', 'local_currency' );
function local_currency( $currency ) {
  global $wp_query;
  $currentAuth = $wp_query->get_queried_object(); // Author meta
  $userID = 'user_'.$currentAuth->ID; //important for ACF author ID
  $country = get_field('country', $userID);

  if ( $country == 'US' ) {
    $local_currency = 'USD';
  } else {
    $local_currency = 'CAD';
  }
  return $local_currency;
}

/*
 * Dynamically populate Registration form
 */


add_filter( 'gform_pre_render_76', 'populate_registration_form' );
add_filter( 'gform_pre_validation_76', 'populate_registration_form' );
add_filter( 'gform_pre_submission_filter_76', 'populate_registration_form' );
add_filter( 'gform_admin_pre_render_76', 'populate_registration_form' );

function populate_registration_form( $form ) {

  $classesListSelect = array();
  $pricesListSelect = array();
  $methodsListSelect = array();
  $waiverListSelect = array();

  global $wp_query;
  $currentAuth = $wp_query->get_queried_object(); // Author meta
  $userID = 'user_'.$currentAuth->ID; //important for ACF author ID
  $country = get_field('country', $userID);

  if ( $country == 'US' ) {
    $waiver = 'I hereby accept the <a target="_blank" class="text-link" href="'.site_url().'/us-waiver/" target="_blank">Acknowledgement of Risk and Waiver of Liability & Photo Release Waiver</a>';
  } else {
    $waiver = 'I hereby accept the <a target="_blank" class="text-link" href="'.site_url().'/canada-waiver/" target="_blank">Acknowledgement of Risk and Waiver of Liability & Photo Release Waiver</a>';
  }

  array_push($waiverListSelect, array('value' => $waiver, 'text' => 'I hereby accept the Acknowledgement of Risk and Waiver of Liability & Photo Release Waiver' ) );

  $registration_start_date = get_field('registration_start_date','option');
  $registration_end_date = get_field('registration_end_date','option');

  $today = date("U");

  $todayFormatted = date("Ymd");

  $sessions = array();

  // Pull out all Sessions that have not ended
  $args = array(
    'post_type' => 'session',
    'author' => $currentAuth->ID,
    'post_status' => 'publish',
    'posts_per_page' => 3,
    'orderby' => 'date',
    'order' => 'DESC',
    'meta_query' => array(
      array(
        'key' => 'end_date', // Check 'end_date'
        'value' => $todayFormatted, // Compare to today
        'compare' => '>=',
        'type' => 'DATE'
      )
    )
  );
  $sessionArray = new WP_Query( $args );

  if ( $sessionArray->have_posts() ) :
    while ( $sessionArray->have_posts() ) :
      $sessionArray->the_post();

      // For each session, check if session has any classes
      $classes = get_posts(array(
        'post_type' => 'class',
        'author' => $currentAuth->ID,
        'meta_query' => array(
          array(
            'key' => 'session',
            'value' => get_the_ID(),
            'compare' => 'LIKE'
          )
        ),
        'posts_per_page'  => 1,
      ));

      // If session has classes, then display it as an option in the Dropdown list of Sessions
      if( $classes ):
        $sessions[] = get_the_ID();
        //error_log('session with classes = '.$post->ID);
      endif;
    endwhile;
    wp_reset_postdata();
  endif;

  $meta_query = array('relation' => 'OR');
  foreach ($sessions as $value) {
    $meta_query[] = array(
      'key'       => 'session',
      'value'     => $value,
      'compare'   => 'LIKE',
    );
  }

  /* CLASSES */
  $args = array(
    'post_type'   => 'class',
    'author' => $currentAuth->ID,
    'post_status' => 'publish',
    'meta_query' => array(
      $meta_query,
      'relation' => 'AND',
      array(
        'show_in_registration_form' => array(
          'key'     => 'show_in_registration_form',
          'value'   => 1,
          'compare' => 'LIKE'
        )
      ),
    ),
    'orderby'     => array( 'session' => 'DESC', 'day_of_the_week' => 'ASC', 'start_time' => 'ASC', 'end_time' => 'ASC' ),
    'posts_per_page'  => -1,
  );

  $classes = new WP_Query($args);

  if ( $classes->have_posts() ) :

    while ( $classes->have_posts() ) :
      $classes->the_post();

      $curSessions = get_field('session',  get_the_ID() );

      if( $curSessions ):
        foreach( $curSessions as $curSession):
          $session_name = get_the_title($curSession->ID);
          $session_start_date = date('U', strtotime(get_field('start_date',$curSession->ID)));
          $session_end_date = date('U', strtotime(get_field('end_date',$curSession->ID)));


          if ( ($today >= $registration_start_date && $today <= $registration_end_date) ) {

            $day = get_field('day_of_the_week',get_the_ID());
            $date_time = strtoupper(date('D', strtotime($day['label'])).' '.get_field('start_time', get_the_ID()).' - '.get_field('end_time', get_the_ID()));

            $levels = get_the_terms( get_the_ID(), 'level' );
            if(!empty($levels)){
              foreach($levels as $level){
                $class_name = $level->name;

              }
            }

            $teachers = get_field('teacher', get_the_ID() );
            if( $teachers ):
              $i = 0;
              foreach( $teachers as $teacher):
                $i++;
                if ($i==1) {
                  $teacher_list = get_the_title($teacher->ID);
                } else {
                  $teacher_list .= ' & '.get_the_title($teacher->ID);
                }


              endforeach;
              wp_reset_postdata();
            endif;

            $classesArray = $session_name.' • '.$date_time.' • '.$class_name.' • '.$teacher_list;
            array_push($classesListSelect, array('text' => $classesArray, 'value' => get_the_ID() ) );
          }

        endforeach;
        // var_dump("--------------");
        // var_dump($classesListSelect);
        // var_dump("--------------");
        wp_reset_postdata();
      endif;
    endwhile;
  else:
    error_log('Cannot find classes');
  endif;

  $prices = get_field( 'pricing', 'option' );

  /* PRICES */
  $prices = get_field( 'pricing', 'option' );

  if( have_rows('pricing', 'option') ):

    $i = 0;
    while( have_rows('pricing', 'option') ): the_row();
      if (get_sub_field('show_in_form') == 1) {

        $i++;

        $start_date = get_sub_field('start_date');
        $end_date = get_sub_field('end_date');

        if ( $today >= $start_date && $today <= $end_date ) {
          $pricesArray = get_sub_field('title');

          if ($pricesArray == 'Full year • 3 sessions') {
            if ( $currentAuth->ID == 21 ) {
              continue;
            }
          }
          array_push($pricesListSelect, array('text' => $pricesArray, 'value' => $pricesArray.' • $'.get_sub_field('value').'.00' ) );

          if ($i == 1) {
            $packagePrice = get_sub_field('value');
            $packageName = get_sub_field('title');
          }
        }
      }
    endwhile;
  endif;

  $methods = get_field( 'payment_methods', $userID );
  if( $methods ):
    foreach( $methods as $method ):
      if ($method != 'External') {
        array_push($methodsListSelect, array('text' => $method, 'value' => $method) );
      }
    endforeach;
  endif;

  /* PAYMENT METHODS */
  foreach ( $form['fields'] as &$field ) {

    if (!is_admin()){

      /* CLASSES */
      if ( $field->inputName == 'classes-list' ) {
        $field->placeholder = 'Choose one to register';
        $field->choices = [];
        $field->choices += $classesListSelect;
        // var_dump("field-choices");
        // var_dump($field->choices);
        // var_dump("-----");
      }



      /* PRICES */
      if ( $field->inputName == 'prices-list' ) {
        $field->placeholder = 'Choose one to register';
        $field->choices = [];
        $field->choices += $pricesListSelect;
      }

      /* METHODS */
      if ( $field->inputName == 'methods-list' ) {
        $field->choices = [];
        $field->choices += $methodsListSelect;
      }

      if($field->id == 49){ // Update sub amount
        $field->label = $packageName;
        $field->basePrice = $packagePrice;
      }

      /* WAIVER */
      if ( $field->inputName == 'waiver-link' ) {
        //$field->choices = [];
        $field->choices += $waiverListSelect;
      }

    }
  }

  return $form;
}

/* For external registration system */
add_filter( 'gform_pre_render_82', 'populate_waiver_form' );
add_filter( 'gform_pre_validation_82', 'populate_waiver_form' );
add_filter( 'gform_pre_submission_filter_82', 'populate_waiver_form' );
add_filter( 'gform_admin_pre_render_82', 'populate_waiver_form' );

function populate_waiver_form( $form ) {

  $waiverListSelect = array();

  global $wp_query;
  $currentAuth = $wp_query->get_queried_object(); // Author meta
  $userID = 'user_'.$currentAuth->ID; //important for ACF author ID
  $country = get_field('country', $userID);

  if ( $country == 'US' ) {
    $waiver = 'I hereby accept the <a class="text-link" href="'.site_url().'/us-waiver/" target="_blank">Acknowledgement of Risk and Waiver of Liability & Photo Release Waiver</a>';
  } else {
    $waiver = 'I hereby accept the <a class="text-link" href="'.site_url().'/canada-waiver/" target="_blank">Acknowledgement of Risk and Waiver of Liability & Photo Release Waiver</a>';
  }

  array_push($waiverListSelect, array('value' => $waiver, 'text' => 'I hereby accept the Acknowledgement of Risk and Waiver of Liability & Photo Release Waiver' ) );

  /* PAYMENT METHODS */
  foreach ( $form['fields'] as &$field ) {

    if (!is_admin()){
      /* WAIVER */
      if ( $field->inputName == 'waiver-link' ) {
        $field->choices = [];
        $field->choices += $waiverListSelect;
      }

    }
  }
  return $form;
}

function pre2($val){
    echo "<pre>";
    var_dump($val);
    echo "</pre>";
}

add_filter( 'gform_confirmation', 'custom_confirmation', 10, 4 );
function custom_confirmation( $confirmation, $form, $entry, $ajax ) {
  if( $form['id'] == '82' ) {
    global $wp_query;
    $currentAuth = $wp_query->get_queried_object(); // Author meta
    $userID = 'user_'.$currentAuth->ID; //important for ACF author ID
    $external_link = get_field('external_link', $userID);
    $confirmation = array( 'redirect' => $external_link );
  }
  return $confirmation;
}



/**
 * Change Post Author for Registration form entries
 */
add_filter( 'gform_post_data', 'change_post_author', 10, 3 );
function change_post_author($post_data, $form, $entry){
  //only change post author on form id 76, which is Registration form
  if ( $form['id'] != 76 ) {
    return $post_data;
  }
  //TODO init GW_Inventory and set field id

  // New post author is Location where the form is displayed
  global $wp_query;
  $currentAuth = $wp_query->get_queried_object();
  $post_data['post_author'] = $currentAuth->ID;
  return $post_data;
}


add_filter( 'gform_field_validation_10_2', function ( $result, $value, $form, $field ) {
    $master = rgpost( 'input_1' );
    if ( $result['is_valid'] && $value == $master ) {
        $result['is_valid'] = false;
        $result['message']  = 'Please enter a different email.';
    }

    return $result;
}, 10, 4 );

/**
 * Add Entry IDs for Registration form entries
 */
add_action('gform_after_submission_76', 'save_entry_id', 10, 2);
function save_entry_id($entry, $form){
  update_post_meta($entry['post_id'], 'gfentryid', $entry['id']);
  //TODO Count Cap
  // $test= rgar($entry,57);
  // echo "-------------------dada";
  // var_dump($test);


foreach ($form['fields'] as $field) {
  if($field->label == "Coupon Code") {
    $dId = $field['id'];
  }
}


foreach ($form['fields'] as $field) {
  if($field->label == "Promo code") {
    $couponID = $field['id'];

    // echo "------Coupon";
    $coupon_codes = gf_coupons()->get_submitted_coupon_codes( $form, $entry );
    // pre2($dId);
    // pre2($entry['id']);

// var_dump($dId);

    if($dId > 0 && !empty($coupon_codes)) {
      GFAPI::update_entry_field($entry['id'], $dId, $coupon_codes[0] );
      update_field('registration_promo_code', $coupon_codes[0], $entry['post_id']);
    }

  }
}





  $countCap = (int) get_field('class_cap',$classID);
  $numberCap = (int) get_field('n_students',$classID);
  if ($countCap != $numberCap) {
    $numberCap++;
    update_field('n_students', $numberCap);
  }
}

add_filter('acf/load_field/name=registration_promo_code', 'sampleFunction');
function sampleFunction( $field ){
  $field['disabled']='1';
  return $field;
}
add_filter('acf/load_field/name=class_id', 'block_class_id_edit');
function block_class_id_edit( $field ){
  $field['disabled']='1';
  return $field;
}

/*
 * Update payment status for Registration entries
 */
add_action( 'gform_post_payment_status', 'update_registration_entry_payment_status', 10, 8 );
function update_registration_entry_payment_status($feed, $entry, $status, $transaction_id, $subscriber_id, $amount, $pending_reason, $reason) {
  update_post_meta($entry['post_id'], 'registration_payment_status', $status);
  update_post_meta($entry['post_id'], 'transaction_id', $transaction_id);
  update_post_meta($entry['post_id'], 'registration_notes', $pending_reason.' / '.$reason);
}

/*
 * Check if content is truly empty
 */
function empty_content($str) {
  return trim(str_replace('&nbsp;','',strip_tags($str))) == '';
}


/*
 * Admin Column custom filtering (pulling user's own data only)
 */

// For list of Sessions
function acp_filter_dropdown_sessions( $args, $column ) {

  if ( $column instanceof ACP_Column_CustomField ) {

    if ( 'registration_session' === $column->get_meta_key() ) {
      $args['options'] = $ownSessions = array();

      global $wpdb;
      $result = $wpdb->get_results("SELECT * FROM `wp_posts` WHERE `post_author` = ".get_current_user_id()." AND `post_type` LIKE 'session' AND `post_status` LIKE 'publish'  ");

      foreach($result as $row) {
        $temp = $row->post_title;
        $ownSessions[$temp] = $temp;
      }

      $args['options']  = array_unique($ownSessions);
    }
  }
  return $args;
}
add_filter( 'acp/filtering/dropdown_args', 'acp_filter_dropdown_sessions', 10, 2 );


// For list of Classes
function acp_filter_dropdown_classes( $args, $column ) {

  if ( $column instanceof ACP_Column_CustomField ) {

    if ( 'registration_class_level' === $column->get_meta_key() ) {
      $args['options'] = $ownRegistrations = $ownClasses = array();

      global $wpdb;
      $result = $wpdb->get_results("SELECT * FROM `wp_posts` WHERE `post_author` = ".get_current_user_id()." AND `post_type` LIKE 'registration'");
      foreach($result as $row) {
        array_push($ownRegistrations,$row->ID);
      }

      foreach ($ownRegistrations as &$ownRegistrationsID) {
        global $wpdb;
        $result = $wpdb->get_results("SELECT * FROM `wp_postmeta` WHERE `post_id` = ".$ownRegistrationsID." AND `meta_key` LIKE 'registration_class_level'");
        foreach($result as $row) {
          $temp = $row->meta_value;
          $ownClasses[$temp] = $temp;
        }
      }
      $args['options']  = array_unique($ownClasses);
    }
  }
  return $args;
}
add_filter( 'acp/filtering/dropdown_args', 'acp_filter_dropdown_classes', 10, 2 );


// For list of Packages
function acp_filter_dropdown_packages( $args, $column ) {

  if ( $column instanceof ACP_Column_CustomField ) {

    if ( 'registration_package' === $column->get_meta_key() ) {
      $args['options'] = $ownRegistrations = $ownPackages = array();


      global $wpdb;
      $result = $wpdb->get_results("SELECT * FROM `wp_posts` WHERE `post_author` = ".get_current_user_id()." AND `post_type` LIKE 'registration'");
      foreach($result as $row) {
        array_push($ownRegistrations,$row->ID);
      }

      foreach ($ownRegistrations as &$ownRegistrationsID) {
        global $wpdb;
        $result = $wpdb->get_results("SELECT * FROM `wp_postmeta` WHERE `post_id` = ".$ownRegistrationsID." AND `meta_key` LIKE 'registration_package'");
        foreach($result as $row) {
          $temp = $row->meta_value;
          $ownPackages[$temp] = $temp;
        }
      }
      $args['options']  = array_unique($ownPackages);
    }
  }
  return $args;
}
add_filter( 'acp/filtering/dropdown_args', 'acp_filter_dropdown_packages', 10, 2 );

// For list of Payment Statuses
function acp_filter_dropdown_statuses( $args, $column ) {

  if ( $column instanceof ACP_Column_CustomField ) {

    if ( 'registration_payment_status' === $column->get_meta_key() ) {
      $args['options'] = $ownRegistrations = $ownStatuses = array();


      global $wpdb;
      $result = $wpdb->get_results("SELECT * FROM `wp_posts` WHERE `post_author` = ".get_current_user_id()." AND `post_type` LIKE 'registration'");
      foreach($result as $row) {
        array_push($ownRegistrations,$row->ID);
      }

      foreach ($ownRegistrations as &$ownRegistrationsID) {
        global $wpdb;
        $result = $wpdb->get_results("SELECT * FROM `wp_postmeta` WHERE `post_id` = ".$ownRegistrationsID." AND `meta_key` LIKE 'registration_payment_status'");
        foreach($result as $row) {
          $temp = $row->meta_value;
          $ownStatuses[$temp] = $temp;
        }
      }
      $args['options']  = array_unique($ownStatuses);
    }
  }
  return $args;
}
add_filter( 'acp/filtering/dropdown_args', 'acp_filter_dropdown_statuses', 10, 2 );


// For list of Notes in Classes
function acp_filter_dropdown_notes( $args, $column ) {

  if ( $column instanceof ACP_Column_CustomField ) {

    if ( 'note' === $column->get_meta_key() ) {
      $args['options'] = $ownRegistrations = $ownNotes= array();


      global $wpdb;
      $result = $wpdb->get_results("SELECT * FROM `wp_posts` WHERE `post_author` = ".get_current_user_id()." AND `post_type` LIKE 'class'");
      foreach($result as $row) {
        array_push($ownRegistrations,$row->ID);
      }

      foreach ($ownRegistrations as &$ownRegistrationsID) {
        global $wpdb;
        $result = $wpdb->get_results("SELECT * FROM `wp_postmeta` WHERE `post_id` = ".$ownRegistrationsID." AND `meta_key` LIKE 'note'");
        foreach($result as $row) {
          $temp = $row->meta_value;
          $ownNotes[$temp] = $temp;
        }
      }
      $args['options']  = array_unique($ownNotes);
    }
  }
  return $args;
}
add_filter( 'acp/filtering/dropdown_args', 'acp_filter_dropdown_notes', 10, 2 );

/**
 * In Admin, list Registration Entries latest first
 **/

function set_post_order_in_admin( $wp_query ) {
  if ( (is_admin() || is_post_type_archive('registration')) && $wp_query->query_vars['post_type'] == 'registration' ) {
    $wp_query->set( 'orderby', 'publish_date' );
    $wp_query->set( 'order', 'DESC' );
    $wp_query->set( 'posts_per_page', '20' );
  }
}
add_filter('pre_get_posts', 'set_post_order_in_admin' );
