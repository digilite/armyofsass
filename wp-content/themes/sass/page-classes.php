<?php
/**
 * Template Name: Full width
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sass
 */

get_header(); 

$countries = array('canada','us');

?>
	
	<div class="bow-black-magenta"></div>

	<div id="primary" class="row expanded collapse">
		<main id="main" class="small-12 columns" role="main">

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<header class="entry-header">
					<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
				</header><!-- .entry-header -->

				<div class="entry-content">
					<?php the_content(); ?>
					
					<?php 
					if ( count($countries) > 1 ) :
						echo '<ul class="tabs text-center" data-tabs id="countries">';

						$i = 0;
						foreach ( $countries as $country ) :
							$i++;
							if ($i == 1) { $default = " is-active"; } else { $default=""; }
							echo '<li class="tabs-title'.$default.'"><a class="button" href="#'.$country.'" aria-selected="true">'.$country.'</a></li>';
						endforeach;

						echo '</ul>';

						echo '<div class="tabs-content" data-tabs-content="countries">';

						foreach ( $countries as $country ) : 
							echo '<div class="tabs-panel is-active" id="'.$country.'">'; 
						?>

						<div class="grid-classes">
							<div class="row expanded collapse small-up-2 medium-up-3 large-up-4 xlarge-up-6" data-equalizer data-equalizer-by-row="true">
								<?php 
								$args = array(
									'role'         => 'location_user',
									'exclude'      => array(2,25,27,28,1043), //Admin, lannie, aos, etobicoke, aos-us
									'orderby'      => 'user_nicename',
									'order'        => 'ASC',
									'meta_key' => 'country', 
									'meta_value' => $country,
									'meta_compare'  => '=' 
								); 
								$wp_user_query = new WP_User_Query( $args ); 

								$locations = $wp_user_query->get_results();

								foreach ( $locations as $location ) { 
									
									echo '<div class="column column-block card" data-equalizer-watch>';
									echo '<div class="front">';
									echo '<h3>';
									echo 'AOS '.get_field('location_name','user_'.$location->ID).'<br /><small class="caps">'.get_field('province_state','user_'.$location->ID).' • '.get_field('country','user_'.$location->ID).'</small></h3>';

									$classes = get_posts(array(
										'post_type' 	=> 'class',
										'author' => $location->ID,
								    'post_status' => 'publish',
										'posts_per_page'	=> -1,
									));

									$totalClasses = count($classes);

									if ( $totalClasses == 0 ) {
										$classResult = 'Classes TBA';
									} elseif ( $totalClasses == 1 ) {
										$classResult = $totalClasses.' class';
									} else {
										$classResult = $totalClasses.' classes';
									}

									echo '<h5><span class="subheader">'.$classResult.'</span></h5>';
									echo '<a class="button" href="../' . esc_html( $location->user_nicename ) . '/">More info ›</a>';
									echo '</div>';
									echo '</div>';
								}
							?>
							
							</div>
						</div>

						<?php
							echo '</div>';
						endforeach;
						echo '</div>';
					endif;
					?>

					<div class="section"></div>

				</div><!-- .entry-content -->
			</article><!-- #post-## -->


			


		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
