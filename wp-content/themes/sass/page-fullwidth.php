<?php
/**
 * Template Name: Full width
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sass
 */

get_header(); ?>
	
	<div class="bow-black-magenta"></div>

	<div id="primary" class="row expanded">
		<main id="main" class="small-12 columns" role="main">

			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'page' );

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
