<?php
/**
 * Template Name: Heels Dance 101
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sass
 */

get_header(); ?>
	


	<div id="primary" class="row expanded collapse">
		<main id="main" class="small-12 columns" role="main">

			<?php
			while ( have_posts() ) : the_post(); ?>

				<!-- Banner slider -->
				<?php if( have_rows('banners' )): ?>

					<div class="slider">
						<div class="slick">

							<?php 
							while( have_rows('banners') ): 
								the_row(); 
								$image = get_sub_field('image'); 
								$heading = get_sub_field('heading'); 
								$cta = get_sub_field('cta');
								$link = get_sub_field('link');
								
								if (get_sub_field('button_theme') == 1 ) {
									$button_theme = 'light';
									$blurb_theme = 'dark';
								} else {
									$button_theme = 'dark';
									$blurb_theme = 'light';
								}

							?>
							<div class="">
						  	<div class="banner" style=" background-image: url('<?php echo $image['url']; ?>');background-position:bottom center;">
						  		<div class="content-wrapper">
						  			<div class="vcentered full">
						  				
							  			<p class="blurb <?php echo $blurb_theme; ?>"><?php echo $heading; ?></p>
							  		
							  			<?php if ($cta) { ?> 
							  			<a class="button <?php echo $button_theme; ?>" href="<?php echo $link; ?>"><?php echo $cta; ?></a>
							  			<?php } ?>
						  			</div>
						  		</div>
						  	</div>
					  	</div>
							<?php endwhile; ?>
						
						</div>

						<div class="mask-container"><div class="mask bow-grey-white"></div></div>
					</div>
				  <?php endif; ?>


				  <div class="row">
						<div class="small-12 medium-10 medium-centered large-8 xlarge-6 columns">
							<?php get_template_part( 'template-parts/content', 'page' ); ?>
						</div>
				  </div>
					
					<div class="triangle-rise-black"></div>
					<div class="section dark" id="team">
						<div class="row">
				  		<div class="small-12 columns text-center">
				  			<h1 class="caps">• <?php _e('Teachers','sass');?> •</h1>
				  		</div>
				  	</div>

				  	<div class="row expanded collapse" data-equalizer>
							<?php $teachers = get_field('teachers'); ?>
							<?php if( $teachers ): ?>
								<?php foreach( $teachers as $teacher ): ?>
					
									<div class="small-12 medium-4 large-3 columns card end">
										<?php

										$photo = wp_get_attachment_image_src(get_post_thumbnail_id($teacher->ID), 'team-thumb')[0];
										$photo_full = wp_get_attachment_image_src(get_post_thumbnail_id($teacher->ID), 'team-full')[0];

										if (!$photo) {
										 $photo = get_template_directory_uri().'/img/avatar.gif';
										}

										?>
										<div class="front" style="background-image: url('<?php echo $photo; ?>');" data-equalizer-watch>
											<div class="info">
												<?php
												echo '<h2>'.get_the_title( $teacher->ID ).'</h2><br />';
												$position = wp_get_post_terms($teacher->ID, 'position', array("fields" => "all"));
												$locationID = $teacher->post_author;
												echo '<h3>'.$position[0]->name.'</h3><br />';
												echo '<h4>'.get_field('location_name','user_'.$locationID).' • '.get_field('province_state','user_'.$locationID).' • <span class="caps">'.get_field('country','user_'.$locationID).'</span></h4>';
												?>
											</div>
										</div>
										<div class="back">
											<?php 

											$email = get_field('email', $teacher->ID);
											if ( !$email ) {
												$email = 'admin@armyofsass.com';
											} ?>
											<a class="button hollow" href="mailto:<?php echo $email; ?>?subject=AOS // Heels Dance 101">Email</a>
										</div>
									</div>
										
									<?php endforeach; ?>
									
								<?php endif; ?>

					  	</div>
					  </div>
					 </div>
					 <div class="bow-black-cyan"></div>

			<?php  endwhile; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
