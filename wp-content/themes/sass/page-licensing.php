<?php
get_header(); 

while ( have_posts() ) : the_post(); ?>
	
	<!-- Banner slider -->
	<?php if( have_rows('banners' )): ?>

		<div class="slider">
			<div class="slick">

				<?php 
				while( have_rows('banners') ): 
					the_row(); 
					$image = get_sub_field('image'); 
					$big_heading = get_sub_field('big_heading'); 
					$big_heading_img = get_sub_field('big_heading_stylized'); 
					$cta = get_sub_field('cta');
					$link = get_sub_field('link');
					
					if (get_sub_field('button_theme') == 1 ) {
						$button_theme = 'light';
						$blurb_theme = 'dark';
					} else {
						$button_theme = 'dark';
						$blurb_theme = 'light';
					}

				?>
				<div class="">
			  	<div class="banner" style=" background-image: url('<?php echo $image['url']; ?>');background-position:bottom center;">
			  		<div class="content-wrapper">
			  			<div class="vcentered full">
			  				<?php if($big_heading_img) { ?>
			  					<p class="text-center"><img class="inline" src="<?php echo $big_heading_img['url']; ?>" alt="<?php echo $big_heading; ?>" /></p>
			  				<?php } else { ?>
				  			<p <?php echo $blurb_theme; ?>"><?php echo $big_heading; ?></p>
				  			<?php } ?> 
				  			
				  			<?php if ($cta) { ?> 
				  			<a class="button <?php echo $button_theme; ?>" href="<?php echo $link; ?>"><?php echo $cta; ?></a>
				  			<?php } ?>
			  			</div>
			  		</div>
			  	</div>
		  	</div>
				<?php endwhile; ?>
			
			</div>

			<div class="mask-container"><div class="mask bow-grey-white"></div></div>
		</div>
	  <?php endif; ?>


		<!-- Intro -->
	  <div class="section light" id="intro">
	  	<div class="row">
	  		<div class="small-12 columns text-center">
	  			<h1 class="thin caps less-bottom"><?php echo get_field('intro_heading'); ?></h1>
	  		</div>
	  	</div>
	  	<div class="row">
				<div class="small-12 medium-10 medium-centered large-8 xlarge-7 columns text-center">
					<p><strong>•••••</strong></p>
					<h4 class="thin caps"><?php echo get_field('intro'); ?></h4>
					<strong>•••••</strong>
				</div>
	  	</div>
		</div>


		<?php 
		if( have_rows('content_blocks' )):
			$i = 0;
			while( have_rows('content_blocks') ): 
				$i++;
				the_row(); 

				switch ($i) {
			    case 1:
		        $ribbon_class = 'ribbon no-button';
		        $section_heading = '';
		        break;
		      case 2:
		        $ribbon_class = 'ribbon no-button text-left';
		        $section_heading = 'Why Army of Sass?';
		        break;
		    }
		    ?>
	  <!-- Three CTA blocks -->
	  <div class="row">
  		<div class="small-12 columns text-center">
				<br />
				<a href="<?php echo site_url().'/licensing/apply/'; ?>" class="button primary large caps">Apply today!</a>
				<p><small>There is one and only one approved licensee per location. It's first come first serve, so hurry!</small></p>

				<?php if($section_heading) { ?>
  			<br /><h1 class="thin caps less-bottom"><small>•••••</small><br /><?php echo $section_heading; ?></h1>
				<?php } ?>
  		</div>
  	</div>
		<div class="relative">
			<div class="<?php echo $ribbon_class; ?>" data-equalizer>
			  <div class="row expanded">


				<?php 
			  if( have_rows('block' )): 
			  	$k = 0;
			  	while( have_rows('block') ): 
			  		$k++;
			  		the_row(); 

						$image = get_sub_field('image');
						$heading = get_sub_field('heading');
						$blurb = get_sub_field('blurb');

						if ($i == 1) {
							switch ($k) {
						    case 1:
					        $gradient_class = 'PM';
					        break;
					      case 2:
					        $gradient_class = 'MR';
					        break;
					      case 3:
					        $gradient_class = 'RO';
					        break;
					    }
						}

						if($i == 2) {
							switch ($k) {
						    case 1:
					        $gradient_class = 'OY';
					        break;
					      case 2:
					        $gradient_class = 'YG';
					        break;
					      case 3:
					        $gradient_class = 'GC';
					        break;
					    }
					  }
				?>

			    <div class="small-12 large-4 columns"> 
			      <div class="text-center"><img class="gradient <?php echo $gradient_class; ?>" src="<?php echo $image['url']; ?>" alt="<?php echo $heading; ?>" /></div>
			      <div data-equalizer-watch>
			      	<h3 class="caps text-center"><?php echo $heading; ?></h3>
			      	<?php if ($i == 1) {?><p><?php } ?><?php echo $blurb; ?><?php if ($i == 1) {?></p><?php } ?>
			      </div>
			      <hr class="hide-for-large" />
			    </div>

		  	<?php endwhile; endif; ?>
			  </div>
			</div>
		</div>
		<?php endwhile; endif; ?>

		<!-- Contact -->
		<?php 
		if( have_rows('faqs') ):
		?>
		<div class="section light" id="contact">
			<div class="row">
	  		<div class="small-12 columns text-center">
	  			<h1 class="caps less-bottom">• <?php _e('FAQ','sass');?> •</h1>
	  			<h4 class="thin">Check out these frequently asked questions, or <span class="local-scroll"><a class="text-link" href="#contact-form"><strong>contact us</strong></a></span> using the form below.</h4>
	  		</div>
	  	</div>

	  	<div class="row">
	  		<div class="small-12 medium-10 medium-centered large-8 xlarge-6 columns text-center">
					
					<ul class="accordion text-left" data-accordion data-allow-all-closed="true">
			  		
			  		<?php 
						while( have_rows('faqs') ): 
							$i++;
							the_row(); 
							$question = get_sub_field('question'); 
							$answer = get_sub_field('answer');
						?>

					  <li class="accordion-item" data-accordion-item>
					    <a href="#" class="accordion-title"><h3 class="caps"><?php echo $question; ?></h3></a>
					    <div class="accordion-content" data-tab-content>
					      <p><?php echo $answer; ?></p>
					    </div>
					  </li>
					 
					 	<?php endwhile; ?>

					  <li class="accordion-item is-active" data-accordion-item>
					    <a href="#" class="accordion-title" name="contact-form"><h3 class="caps">Questions or comments ? Contact us here !</h3></a>
					    <div class="accordion-content" data-tab-content>
					      <?php echo do_shortcode('[gravityforms id="79" title="false" description="false" ajax="true"]'); ?>
					    </div>
					  </li>
					</ul>

	  		</div>
	  	</div>
	  </div>
		<div class="bow-green-black"></div>
	  <?php endif; ?>

		<!-- Apply -->
		<div class="section light">
		  <div class="row">
	  		<div class="small-12 columns text-center">
					<a href="<?php echo site_url().'/licensing/apply/'; ?>" class="button primary large caps">Apply today!</a>
					<br />
					<p><small>There is one and only one approved licensee per location. It's first come first serve, so hurry!</small></p>
					<br />
	  		</div>
	  	</div>
  	</div>

		

<?php endwhile;

get_footer(); ?>