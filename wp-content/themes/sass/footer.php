<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package sass
 */

?>
		
		<?php if ( !is_page('licensing') && !is_page('licensing/application') ) { ?>

		<?php if ( !is_front_page() && !is_page('heelsdance101') ) { ?><div class="bow-green-black"></div><?php } ?>

		<!-- Newsletter subscription -->
		<div class="section light" id="newsletters">
			<div class="row">
	  		<div class="small-12 columns text-center">
	  			<h1 class="caps thin less-bottom"><?php _e('Be the first to know','sass');?><br /><small>•••••</small></h1>
	  		</div>
	  	</div>

	  	<div class="row">
	  		<div class="small-12 medium-8 medium-centered large-6 xlarge-4 columns text-center">
	  			<p><?php echo get_field('newsletter_subscription_text','option'); ?></p>
					<?php echo do_shortcode('[gravityforms id="78" title="false" description="false" ajax="true" tabindex="300"]'); ?>
	  		</div>
	  	</div>

	  	<p>&nbsp;</p>
	  </div>

	  <?php } ?>

		<footer class="section dark even">
			<div class="row">
				<div class="small-12 text-center">

					<?php if ( is_author() ) {
						global $wp_query;
						$currentAuth = $wp_query->get_queried_object(); // Author/location object
						
						$userID = 'user_'.$currentAuth->ID; //important for ACF author ID
						$locationName = get_field('location_name', $userID);

						$facebook_url = get_field('facebook_url',$userID);
						$instagram_url = get_field('instagram_url',$userID);
					} else {
						$facebook_url = get_field('facebook_url','option');
						$instagram_url = get_field('instagram_url','option');
					} ?>

					<?php if ( $facebook_url || $instagram_url ) { ?>

					<h2 class="caps">Follow AOS<?php if(is_author()) { echo ' '.$locationName; } ?></h2>
					<?php if ( $facebook_url ) { ?>
					<a class="icon-facebook" target="_blank" href="<?php echo $facebook_url; ?>"><span>Facebook</span></a>
					<?php } ?>
					
					<?php if ( $instagram_url ) { ?>
					<a class="icon-instagram" target="_blank" href="<?php echo $instagram_url; ?>"><span>Instagram</span></a>
					<?php } ?>

					<?php }  ?>
					<br /><br /><br />
					<small>2010 - <?php echo date('Y'); ?> &copy; Army of Sass. All rights reserved.</small>
				</div>
			</div>
		</footer>

	</div><!-- #content -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
