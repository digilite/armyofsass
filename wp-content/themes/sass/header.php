<?php
/**
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package sass
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
<?php if (is_author()) {
	global $wp_query;
  $currentAuth = $wp_query->get_queried_object(); // Author meta
  $userID = 'user_'.$currentAuth->ID; //important for ACF author ID
  $locationName = get_field('location_name', $userID);
?>
<title>ARMY of SASS in <?php echo $locationName; ?> • Heels Dance Training &amp; Performance Program</title>
<?php } else {  ?>
<title><?php wp_title('&bull;'); ?></title>
<?php } ?>
<!--[if gte IE 9]>
  <style type="text/css">
    .gradient {
       filter: none;
    }
  </style>
<![endif]-->
<?php wp_head(); ?>

<!-- Google Analytics -->
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-5069684-30']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

<!-- Facebook Pixel -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '477258749131048');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=477258749131048&ev=PageView&noscript=1"
/></noscript>

</head>

<body <?php body_class('no-js'); ?>>

<div id="page" class="site">

	<header id="masthead" role="banner" class="top-nav nav-down <?php if (is_author()) {?>white<?php } else { ?>black<?php } ?>">

		<div class="accent gradient <?php if (is_author()) {?> CGY<?php } else { ?> MP<?php } ?>"></div>

		<div class="title-bar" data-responsive-toggle="menu-mobile" data-hide-for="large">
		  <button class="menu-icon <?php if (is_author()) {?>white<?php } else { ?>black<?php } ?>" type="button" data-toggle></button>
		  <div class="title-bar-title">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo get_template_directory_uri().'/img/logo-Army-of-Sass-AOS-';?><?php if (is_author()) {?>black<?php } else { ?>white<?php } ?>.svg" alt="<?php bloginfo( 'name' ); ?>" /></a>
		  </div>
		</div>

		<div class="row collapse hide-for-large" id="menu-mobile" data-animate="fade-in fade-out">
			<div class="small-12 columns">
	      <nav role="primary">
	      	<?php if( is_author() ) {
						//echo '<a class="button primary" data-open="registration">'.__('Register now', 'sass').'</a>';
						wp_nav_menu( array( 'theme_location' => 'menu-location', 'menu_id' => 'menu-location', 'menu_class' => 'vertical menu', 'container' => 'ul') );
					} else {
						echo '<a class="button primary" href="'.site_url().'/classes/">'.__('Find classes', 'sass').'</a>';
						wp_nav_menu( array( 'theme_location' => 'menu-primary', 'menu_id' => 'menu-primary', 'menu_class' => 'vertical menu', 'container' => 'ul') );
					}
					?>
				</nav>
			</div>
		</div>

		<div class="row collapse hide-for-small-only hide-for-medium-only" id="menu-main">
		  <div class="small-6 medium-3 large-2 columns">
      	<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo get_template_directory_uri().'/img/logo-Army-of-Sass-AOS-';?><?php if (is_author()) {?>black<?php } else { ?>white<?php } ?>.svg" alt="<?php bloginfo( 'name' ); ?>" /></a>
			</div>
			<div class="small-6 medium-9 large-10 columns">
	      <nav role="primary" class="clearfix">
					<?php if( is_author() ) {
						//echo '<a class="button primary float-right" data-open="registration">'.__('Register now', 'sass').'</a>';
						wp_nav_menu( array( 'theme_location' => 'menu-location', 'menu_id' => 'menu-location', 'menu_class' => 'menu float-right') );
					} else {
						echo '<a class="button primary float-right" href="'.site_url().'/classes/">'.__('Find classes', 'sass').'</a>';
						wp_nav_menu( array( 'theme_location' => 'menu-primary', 'menu_id' => 'menu-primary', 'menu_class' => 'menu float-right' ) );
					}
					?>
				</nav>
			</div>
		</div>

	</header><!-- #masthead -->

	<div id="content" class="site-content">
