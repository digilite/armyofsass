<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package sass
 */

get_header(); ?>

	
	<div class="bow-black-cyan"></div>

	<div id="primary" class="row">
		<main id="main" class="small-12 medium-11 medium-centered large-10 xlarge-7 columns" role="main">

			<section class="section error-404 not-found">
				<header class="page-header">
					<h1 class="entry-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'sass' ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content text-center">
					<?php
						get_search_form();
					?>

				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();