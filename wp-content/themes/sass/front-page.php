<?php
get_header(); 

while ( have_posts() ) : the_post(); ?>
	
	<!-- Banner slider -->
	<?php if( have_rows('banners' )): ?>

		<div class="slider">
			<div class="slick">

				<?php 
				while( have_rows('banners') ): 
					the_row(); 
					$image = get_sub_field('image'); 
					$big_heading = get_sub_field('big_heading'); 
					$small_heading_img = get_sub_field('small_heading_stylized'); 
					$small_heading = get_sub_field('small_heading'); 
					$cta = get_sub_field('cta');
					$link = get_sub_field('link');
					
					if (get_sub_field('button_theme') == 1 ) {
						$button_theme = 'light';
						$blurb_theme = 'dark';
					} else {
						$button_theme = 'dark';
						$blurb_theme = 'light';
					}

				?>
				<div class="">
			  	<div class="banner" style=" background-image: url('<?php echo $image['url']; ?>');background-position:bottom center;">
			  		<div class="content-wrapper">
			  			<div class="vcentered full">
			  				<p class="blurb <?php echo $blurb_theme; ?>"><?php echo $big_heading; ?></p>
			  				<?php if($small_heading_img) { ?>
			  					<p class="text-center"><img class="inline" src="<?php echo $small_heading_img['url']; ?>" alt="<?php echo $small_heading; ?>" /></p>
			  				<?php } else { ?>
				  			<p <?php echo $blurb_theme; ?>"><?php echo $small_heading; ?></p>
				  			<?php } ?> 
								<?php if ($cta) { ?> 
				  			<a class="button <?php echo $button_theme; ?>" href="<?php echo $link; ?>"><?php echo $cta; ?></a>
				  			<?php } ?>
			  			</div>
			  		</div>
			  	</div>
		  	</div>
				<?php endwhile; ?>
			
			</div>

			<div class="mask-container"><div class="mask bow-grey-white"></div></div>
		</div>
	  <?php endif; ?>


		<!-- Intro -->
	  <div class="section light" id="intro">
	  	<div class="row">
	  		<div class="small-12 columns text-center">
	  			<h1 class="thin caps less-bottom"><?php echo get_field('intro_heading'); ?></h1>
	  		</div>
	  	</div>
	  	<div class="row">
				<div class="small-12 medium-8 medium-centered large-6 xlarge-6 columns text-center caps">
					<strong>•••••</strong>
					<h2 class="thin"><small><?php echo get_field('intro_blurb'); ?></small></h2>
					<strong>•••••</strong>
				</div>
	  	</div>
		</div>


		<?php if( have_rows('main_cta_blocks' )): ?>
		<!-- Three CTA blocks -->
		<div class="relative">
			<div class="ribbon" data-equalizer>
			  <div class="row expanded">

			  	<?php 
					while( have_rows('main_cta_blocks') ): 
						the_row(); 
						$image = get_sub_field('image');



						$imageURL = $image['sizes']['team-full'];


						$heading = get_sub_field('heading');
						$blurb = get_sub_field('blurb');
						$button = get_sub_field('button');
						$link = get_sub_field('link');

						$i++;

						switch ($i) {
					    case 1:
				        $btn_class = 'MR';
				        break;
				      case 2:
				        $btn_class = 'RO';
				        break;
				      case 3:
				        $btn_class = 'OY';
				        break;
				    }
					?>

			    <div class="small-12 large-4 columns"> 
			      <img class="gradient <?php echo $btn_class; ?>" src="<?php echo $imageURL; ?>" alt="<?php echo $heading; ?>" />
			      <div data-equalizer-watch>
			      	<h3 class="caps"><?php echo $heading; ?></h3>
			      	<p><?php echo $blurb; ?></p>
			      </div>
			      <a class="button slanted gradient <?php echo $btn_class; ?>" href="<?php echo $link; ?>"><?php echo $button; ?></a>
			      <hr class="hide-for-large" />
			    </div>

			  	<?php endwhile; ?>
			  </div>
			</div>
		</div>
	<?php endif; ?>

<?php endwhile;

get_footer(); ?>