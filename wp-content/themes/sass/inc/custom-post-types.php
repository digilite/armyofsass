<?php

/* Team */
function cpt_team() {
	register_post_type( 'team',
		array('labels' => array(
			'name' => __('Team', 'sass'),
			'singular_name' => __('Team Member', 'sass'),
			'all_items' => __('All Team Members', 'sass'),
			'add_new' => __('Add New', 'sass'),
			'add_new_item' => __('Add New Team Member', 'sass'),
			'edit' => __( 'Edit', 'sass' ),
			'edit_item' => __('Edit Team Member', 'sass'),
			'new_item' => __('New Team Member', 'sass'),
			'view_item' => __('View Team Member', 'sass'),
			'search_items' => __('Search Team Members', 'sass'),
			'not_found' =>  __('No team members yet. Click \'Add New\' above to add one now.', 'sass'),
			'not_found_in_trash' => __('Nothing found in Trash', 'sass'),
			'parent_item_colon' => ''
			),
			'description' => __( 'This is a list of Team Members', 'sass' ),
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => true,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 4,
			'menu_icon' => 'dashicons-groups',
			'rewrite'	=> array( 'slug' => 'team', 'with_front' => false ),
			'has_archive' => 'members',
			'capability_type' => array('member','members'),
			'map_meta_cap'        => true,
			'hierarchical' => false,
			'supports' => array( 'title','editor', 'thumbnail')
	 	)
	);
}
add_action( 'init', 'cpt_team');

register_taxonomy( 'position',
	array('team'),
	array('hierarchical' => true,
		'labels' => array(
			'name' => __( 'Positions', 'sass' ),
			'singular_name' => __( 'Position', 'sass' ),
			'search_items' =>  __( 'Search Positions', 'sass' ),
			'all_items' => __( 'All Positions', 'sass' ),
			'parent_item' => __( 'Parent Position', 'sass' ),
			'parent_item_colon' => __( 'Parent Position:', 'sass' ),
			'edit_item' => __( 'Edit Position', 'sass' ),
			'update_item' => __( 'Update Position', 'sass' ),
			'add_new_item' => __( 'Add New Position', 'sass' ),
			'new_item_name' => __( 'New Position Name', 'sass' )
		),
		'show_ui' => true,
		'query_var' => true,
		'show_admin_column' => true,
		'capabilities' => array (
      'manage_terms' => 'manage_categories',
      'edit_terms' => 'manage_categories',
      'delete_terms' => 'manage_categories',
      'assign_terms' => 'edit_members'
    ),
	)
);

add_filter( 'featured_image_column_default_image', 'team_custom_featured_image_column_image' );
add_filter( 'manage_edit-team_columns', 'set_custom_edit_team_columns' );
add_filter( 'manage_team_posts_columns', 'set_custom_edit_team_columns' );
add_action( 'manage_team_posts_custom_column' , 'custom_team_columns', 10, 2 );

function set_custom_edit_team_columns($columns) {
	unset( $columns['wpseo-score'] );
  unset( $columns['wpseo-score-readability'] );
	unset( $columns['wpseo-title'] );
	unset( $columns['wpseo-metadesc'] );
	unset( $columns['wpseo-focuskw'] );
	$columns['photo'] = __( 'Photo', 'sass' );
	$columns = array(
    'cb' => '<input type="checkbox" />',
    'photo' => 'Photo',
    'title' => 'Name',
    'taxonomy-position' => 'Position',
    'date' => 'Date'
 	);
  return $columns;
}

function custom_team_columns( $column, $post_id ) {
  switch ( $column ) {
    case 'photo' :
    	if ( has_post_thumbnail() ) {
		  	the_post_thumbnail( 'thumbnail' );
		  } else {
		  	echo '<img width="150" height="150" src="'.get_template_directory_uri().'/img/avatar.gif" class="attachment-thumbnail size-thumbnail wp-post-image">';
		  }
	  break;
  }
}


/* Places */
function cpt_place() {
	register_post_type( 'place',
		array('labels' => array(
			'name' => __('Places', 'sass'),
			'singular_name' => __('Place', 'sass'),
			'all_items' => __('All Places', 'sass'),
			'add_new' => __('Add New', 'sass'),
			'add_new_item' => __('Add New Place', 'sass'),
			'edit' => __( 'Edit', 'sass' ),
			'edit_item' => __('Edit Place', 'sass'),
			'new_item' => __('New Place', 'sass'),
			'view_item' => __('View Place', 'sass'),
			'search_items' => __('Search Places', 'sass'),
			'not_found' =>  __('No places yet. Click \'Add New\' above to add one now.', 'sass'),
			'not_found_in_trash' => __('Nothing found in Trash', 'sass'),
			'parent_item_colon' => ''
			),
			'description' => __( 'This is a list of Studios & Venues', 'sass' ),
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => true,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 4,
			'menu_icon' => 'dashicons-location',
			'rewrite'	=> array( 'slug' => 'place', 'with_front' => false ),
			'has_archive' => 'places',
			'capability_type' => array('place','places'),
			'map_meta_cap'        => true,
			'hierarchical' => false,
			'supports' => array( 'title')
	 	)
	);
}
add_action( 'init', 'cpt_place');

register_taxonomy( 'place-type',
	array('place'),
	array('hierarchical' => true,
		'labels' => array(
			'name' => __( 'Types', 'sass' ),
			'singular_name' => __( 'Type', 'sass' ),
			'search_items' =>  __( 'Search Types', 'sass' ),
			'all_items' => __( 'All Types', 'sass' ),
			'parent_item' => __( 'Parent Type', 'sass' ),
			'parent_item_colon' => __( 'Parent Type:', 'sass' ),
			'edit_item' => __( 'Edit Type', 'sass' ),
			'update_item' => __( 'Update Type', 'sass' ),
			'add_new_item' => __( 'Add New Type', 'sass' ),
			'new_item_name' => __( 'New Type Name', 'sass' )
		),
		'show_ui' => true,
		'query_var' => true,
		'show_admin_column' => true,
		'capabilities' => array (
      'manage_terms' => 'manage_categories',
      'edit_terms' => 'manage_categories',
      'delete_terms' => 'manage_categories',
      'assign_terms' => 'edit_places'
    ),
	)
);

add_filter( 'manage_edit-place_columns', 'set_custom_edit_place_columns' );
add_filter( 'manage_place_posts_columns', 'set_custom_edit_place_columns' );
add_action( 'manage_place_posts_custom_column' , 'custom_place_columns', 10, 2 );

function set_custom_edit_place_columns($columns) {
  unset( $columns['date'] );
  unset( $columns['wpseo-score'] );
  unset( $columns['wpseo-score-readability'] );
	unset( $columns['wpseo-title'] );
	unset( $columns['wpseo-metadesc'] );
	unset( $columns['wpseo-focuskw'] );
  $columns['address'] = __( 'Address', 'sass' );
	$columns['date'] = __( 'Date', 'sass' );
  return $columns;
}

function custom_place_columns( $column, $post_id ) {
  switch ( $column ) {
    case 'address' :
		  $address = get_post_meta( $post_id , 'address' , true );
		  echo $address['address'];
		  break;
		case 'notes' :
			if ( get_post_meta( $post_id , 'show_in_registration_form' , true ) == 1 ) {
				$registration = "Accepting registration";
			} else {
				$registration = "Not accepting registration";
			}
		  $note = get_post_meta( $post_id , 'note' , true );
		  echo $registration;
		  echo '<br />';
		  echo strtoupper($note);

	  break;
  }
}


/* Sessions */
function cpt_session() {
	register_post_type( 'session',
		array('labels' => array(
			'name' => __('Sessions', 'sass'),
			'singular_name' => __('Session', 'sass'),
			'all_items' => __('All Sessions', 'sass'),
			'add_new' => __('Add New', 'sass'),
			'add_new_item' => __('Add New Session', 'sass'),
			'edit' => __( 'Edit', 'sass' ),
			'edit_item' => __('Edit Session', 'sass'),
			'new_item' => __('New Session', 'sass'),
			'view_item' => __('View Session', 'sass'),
			'search_items' => __('Search Sessions', 'sass'),
			'not_found' =>  __('No sessions yet. Click \'Add New\' above to add one now.', 'sass'),
			'not_found_in_trash' => __('Nothing found in Trash', 'sass'),
			'parent_item_colon' => ''
			),
			'description' => __( 'This is a list of Sessions', 'sass' ),
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => true,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 4,
			'menu_icon' => 'dashicons-calendar',
			'rewrite'	=> array( 'slug' => 'session', 'with_front' => false ),
			'has_archive' => 'sessions',
			'capability_type' => array('session','sessions'),
			'map_meta_cap'        => true,
			'hierarchical' => false,
			'supports' => array( 'title')
	 	)
	);
}
add_action( 'init', 'cpt_session');

add_filter( 'manage_edit-session_columns', 'set_custom_edit_session_columns' );
add_filter( 'manage_session_posts_columns', 'set_custom_edit_session_columns' );
add_action( 'manage_session_posts_custom_column' , 'custom_session_columns', 10, 2 );

function set_custom_edit_session_columns($columns) {
	unset( $columns['wpseo-score'] );
  unset( $columns['wpseo-score-readability'] );
	unset( $columns['wpseo-title'] );
	unset( $columns['wpseo-metadesc'] );
	unset( $columns['wpseo-focuskw'] );
	unset( $columns['date'] );
	$columns['start-date'] = __( 'Start date', 'sass' );
	$columns['end-date'] = __( 'End date', 'sass' );
	return $columns;
}
function custom_session_columns( $column, $post_id ) {
  switch ( $column ) {
  	case 'start-date' :
	  	$startdate = get_post_meta( $post_id , 'start_date' , true );
	  	echo date('F j, Y',strtotime($startdate));
		break;
	  case 'end-date' :
	  	$enddate = get_post_meta( $post_id , 'end_date' , true );
	  	echo date('F j, Y',strtotime($enddate));
		break;
  }
}


/* Classes */
function cpt_class() {
	register_post_type( 'class',
		array('labels' => array(
			'name' => __('Classes', 'sass'),
			'singular_name' => __('Class', 'sass'),
			'all_items' => __('All Classes', 'sass'),
			'add_new' => __('Add New', 'sass'),
			'add_new_item' => __('Add New Class', 'sass'),
			'edit' => __( 'Edit', 'sass' ),
			'edit_item' => __('Edit Class', 'sass'),
			'new_item' => __('New Class', 'sass'),
			'view_item' => __('View Class', 'sass'),
			'search_items' => __('Search Classes', 'sass'),
			'not_found' =>  __('No classes yet. Click \'Add New\' above to add one now.', 'sass'),
			'not_found_in_trash' => __('Nothing found in Trash', 'sass'),
			'parent_item_colon' => ''
			),
			'description' => __( 'This is a list of Classes / Levels', 'sass' ),
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => true,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 4,
			'menu_icon' => 'dashicons-clock',
			'rewrite'	=> array( 'slug' => 'class', 'with_front' => false ),
			'has_archive' => false,
			'capability_type' => array('class','classes'),
			'map_meta_cap'        => true,
			'hierarchical' => false,
			'supports' => array( 'title')
	 	)
	);
}
add_action( 'init', 'cpt_class');

register_taxonomy( 'level',
	array('class'),
	array('hierarchical' => true,
		'labels' => array(
			'name' => __( 'Class Levels', 'sass' ),
			'singular_name' => __( 'Class Level', 'sass' ),
			'search_items' =>  __( 'Search Class Levels', 'sass' ),
			'all_items' => __( 'All Class Levels', 'sass' ),
			'parent_item' => __( 'Parent Class Level', 'sass' ),
			'parent_item_colon' => __( 'Parent Class Level:', 'sass' ),
			'edit_item' => __( 'Edit Class Level', 'sass' ),
			'update_item' => __( 'Update Class Level', 'sass' ),
			'add_new_item' => __( 'Add New Class Level', 'sass' ),
			'new_item_name' => __( 'New Class Level Name', 'sass' )
		),
		'show_ui' => true,
		'query_var' => true,
		'show_admin_column' => true,
		'capabilities' => array (
      'manage_terms' => 'manage_categories',
      'edit_terms' => 'manage_categories',
      'delete_terms' => 'manage_categories',
      'assign_terms' => 'edit_classes'
    ),
	)
);

add_filter( 'manage_edit-class_columns', 'set_custom_edit_class_columns' );
add_filter( 'manage_class_posts_columns', 'set_custom_edit_class_columns' );
add_action( 'manage_class_posts_custom_column' , 'custom_class_columns', 10, 2 );

function set_custom_edit_class_columns($columns) {
  unset( $columns['date'] );
  unset( $columns['taxonomy-level'] );
  unset( $columns['wpseo-score'] );
  unset( $columns['wpseo-score-readability'] );
	unset( $columns['wpseo-title'] );
	unset( $columns['wpseo-metadesc'] );
	unset( $columns['wpseo-focuskw'] );
  $columns['session'] = __( 'Session', 'sass' );
  $columns['level'] = __( 'Level', 'sass' );
  $columns['notes'] = __( 'Notes', 'sass' );
  $columns['date'] = __( 'Date', 'sass' );
  return $columns;
}

function custom_class_columns( $column, $post_id ) {
  switch ( $column ) {

    case 'level' :
      $terms = get_the_term_list( $post_id , 'level' , '' , ',' , '' );
      if ( is_string( $terms ) )
          echo $terms;
      else
          _e( 'Need to select a level', 'sass' );
    break;

    case 'session' :
		  $session = get_post_meta( $post_id , 'session' , true );
		  echo get_the_title($session[0]);
	  break;

		case 'notes' :
			if ( get_post_meta( $post_id , 'show_in_registration_form' , true ) == 1 ) {
				$registration = "Accepting registration";
			} else {
				$registration = "Not accepting registration";
			}
		  $note = get_post_meta( $post_id , 'note' , true );
		  echo $registration;
		  echo '<br />';
		  echo strtoupper($note);
	  break;
  }
}


/* Registrations */
function cpt_registration() {
	register_post_type( 'registration',
		array('labels' => array(
			'name' => __('Registrations', 'sass'),
			'singular_name' => __('Registration', 'sass'),
			'all_items' => __('All Registrations', 'sass'),
			'add_new' => __('Add New', 'sass'),
			'add_new_item' => __('Add New Registration', 'sass'),
			'edit' => __( 'Edit', 'sass' ),
			'edit_item' => __('Edit Registration', 'sass'),
			'new_item' => __('New Registration', 'sass'),
			'view_item' => __('View Registration', 'sass'),
			'search_items' => __('Search Registrations', 'sass'),
			'not_found' =>  __('No registrations yet. You must enable online registration from your \'Profile\' in order to view registrations here.', 'sass'),
			'not_found_in_trash' => __('Nothing found in Trash', 'sass'),
			'parent_item_colon' => ''
			),
			'description' => __( 'This is a list of Registrations', 'sass' ),
			'public' => true,
			'publicly_queryable' => false,
			'exclude_from_search' => true,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 4,
			'menu_icon' => 'dashicons-clipboard',
			'rewrite'	=> array( 'slug' => 'registration', 'with_front' => false ),
			'has_archive' => 'registrations',
			'capability_type' => array('registration','registrations'),
			'map_meta_cap'        => true,
			'hierarchical' => false,
			'supports' => array( 'title')
	 	)
	);
}
add_action( 'init', 'cpt_registration');

add_filter( 'manage_edit-registration_columns', 'set_custom_edit_registration_columns' );
add_filter( 'manage_registration_posts_columns', 'set_custom_edit_registration_columns' );
add_action( 'manage_registration_posts_custom_column' , 'custom_registration_columns', 10, 2 );


function set_custom_edit_registration_columns($columns) {
  unset( $columns['taxonomy-level'] );
  unset( $columns['wpseo-score'] );
  unset( $columns['wpseo-score-readability'] );
	unset( $columns['wpseo-title'] );
	unset( $columns['wpseo-metadesc'] );
	unset( $columns['wpseo-focuskw'] );
  $columns = array(
    'cb' => '<input type="checkbox" />',
    'title' => 'Name',
    'contact' => 'Contact',
    'session' => 'Session',
    'class' => 'Class',
    'package' => 'Package',
    'reg-date' => 'Registered on',
    'payment-status' => 'Payment status',
		// 'testiram' => 'Kupon'
 	);
  return $columns;
}

function custom_registration_columns( $column, $post_id ) {
  switch ( $column ) {
    case 'contact' :
    	echo get_post_meta( $post_id , 'registration_email' , true ).'<br />';
    	echo get_post_meta( $post_id , 'registration_phone' , true );
    break;

    case 'session' :
    	$sessionID = get_post_meta( $post_id , 'registration_session' , true );
    	echo $sessionID;
    break;

		case 'reg-date' :
  	 	echo get_the_date( 'F j, Y • g:ia T', $post_id);
	  break;

  }
}

/* Shows */
function cpt_show() {
	register_post_type( 'show',
		array('labels' => array(
			'name' => __('Shows', 'sass'),
			'singular_name' => __('Show', 'sass'),
			'all_items' => __('All Shows', 'sass'),
			'add_new' => __('Add New', 'sass'),
			'add_new_item' => __('Add New Show', 'sass'),
			'edit' => __( 'Edit', 'sass' ),
			'edit_item' => __('Edit Show', 'sass'),
			'new_item' => __('New Show', 'sass'),
			'view_item' => __('View Show', 'sass'),
			'search_items' => __('Search Shows', 'sass'),
			'not_found' =>  __('No shows yet. Click \'Add New\' above to add one now.', 'sass'),
			'not_found_in_trash' => __('Nothing found in Trash', 'sass'),
			'parent_item_colon' => ''
			),
			'description' => __( 'This is a list of Shows', 'sass' ),
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => true,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 4,
			'menu_icon' => 'dashicons-tickets-alt',
			'rewrite'	=> array( 'slug' => 'show', 'with_front' => false ),
			'has_archive' => 'shows',
			'capability_type' => array('show','shows'),
			'map_meta_cap'        => true,
			'hierarchical' => false,
			'supports' => array( 'title','editor', 'thumbnail')
	 	)
	);
}
add_action( 'init', 'cpt_show');

add_filter( 'manage_edit-show_columns', 'set_custom_edit_show_columns' );
add_filter( 'manage_show_posts_columns', 'set_custom_edit_show_columns' );
add_action( 'manage_show_posts_custom_column' , 'custom_show_columns', 10, 2 );

function set_custom_edit_show_columns($columns) {
	unset( $columns['wpseo-score'] );
  unset( $columns['wpseo-score-readability'] );
	unset( $columns['wpseo-title'] );
	unset( $columns['wpseo-metadesc'] );
	unset( $columns['wpseo-focuskw'] );
	$columns = array(
    'cb' => '<input type="checkbox" />',
    'banner' => 'Banner',
    'title' => 'Name',
    'show-count' => 'Show count',
    'date' => 'Date'
 	);
  return $columns;
}

function custom_show_columns( $column, $post_id ) {
  switch ( $column ) {
  	case 'banner' :
    	if ( has_post_thumbnail() ) {
		  	the_post_thumbnail( 'medium' );
		  } else {
		  	echo 'Need to upload show banner';
		  }
	  break;

	  case 'show-count' :
	  	$showtimes = get_post_meta( $post_id , 'showtimes' , true );
	  	echo count($showtimes);
	  break;
  }
}


/* Spotlight */
function cpt_spotlight() {
	register_post_type( 'spotlight',
		array('labels' => array(
			'name' => __('Spotlight', 'sass'),
			'singular_name' => __('Spotlight', 'sass'),
			'all_items' => __('All Items', 'sass'),
			'add_new' => __('Add New', 'sass'),
			'add_new_item' => __('Add New Item', 'sass'),
			'edit' => __( 'Edit', 'sass' ),
			'edit_item' => __('Edit Item', 'sass'),
			'new_item' => __('New Item', 'sass'),
			'view_item' => __('View Item', 'sass'),
			'search_items' => __('Search Items', 'sass'),
			'not_found' =>  __('No spotlight items yet. Click \'Add New\' above to add one now.', 'sass'),
			'not_found_in_trash' => __('Nothing found in Trash', 'sass'),
			'parent_item_colon' => ''
			),
			'description' => __( 'This is a list of Spotlight Items', 'sass' ),
			'public' => true,
			'publicly_queryable' => false,
			'exclude_from_search' => true,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 4,
			'menu_icon' => 'dashicons-admin-post',
			'rewrite'	=> array( 'slug' => 'spotlight', 'with_front' => false ),
			'has_archive' => 'spotlight',
			'capability_type' => array('item','items'),
			'map_meta_cap'        => true,
			'hierarchical' => false,
			'supports' => array( 'title','editor','thumbnail')
	 	)
	);
}
add_action( 'init', 'cpt_spotlight');

add_filter( 'manage_edit-spotlight_columns', 'set_custom_edit_spotlight_columns' );
add_filter( 'manage_spotlight_posts_columns', 'set_custom_edit_spotlight_columns' );
add_action( 'manage_spotlight_posts_custom_column' , 'custom_spotlight_columns', 10, 2 );

function set_custom_edit_spotlight_columns($columns) {
	unset( $columns['wpseo-score'] );
  unset( $columns['wpseo-score-readability'] );
	unset( $columns['wpseo-title'] );
	unset( $columns['wpseo-metadesc'] );
	unset( $columns['wpseo-focuskw'] );
	$columns = array(
    'cb' => '<input type="checkbox" />',
    'image' => 'Image',
    'title' => 'Title',
    'excerpt' => 'Excerpt',
    'date' => 'Date'
 	);
  return $columns;
}

function custom_spotlight_columns( $column, $post_id ) {
  switch ( $column ) {
  	case 'image' :
    	if ( has_post_thumbnail() ) {
		  	the_post_thumbnail( 'thumbnail' );
		  }
	  break;
	  case 'excerpt' :
	  	the_excerpt();
	  break;
  }
}

?>
