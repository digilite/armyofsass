<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package sass
 */

get_header(); ?>
	
	<div class="bow-black-magenta"></div>

	<div id="primary" class="row">
		<main id="main" class="small-12 medium-11 medium-centered large-10 xlarge-7 columns" role="main">
			<div class="section">

				<?php
				while ( have_posts() ) : the_post();

					get_template_part( 'template-parts/content', get_post_format() );

					/* If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
					*/

				endwhile; // End of the loop.
				?>

			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
