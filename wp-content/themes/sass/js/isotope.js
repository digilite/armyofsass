jQuery(document).ready(function ($) {

	if( $(window).width() <= 768 ) {
		offsetTop = -20;
	} else {
		offsetTop = -50;
	}

	$('ul#menu-location, .location-wrapper .content').localScroll({
		'offset' : { 'top' : offsetTop },
		'hash' : true
	});

	$('.menu.vertical#menu-location li a').click(function() {
    $('.menu-icon').trigger('click');
	});

	$('ul#menu-location a, .menu.vertical#menu-location li a').click(function() {
		$('ul#menu-location li, .menu.vertical#menu-location li').removeClass('current_page_item');
		$(this).parent().addClass('current_page_item');
	});

	/* Set up isotope grid for Classes on Location page */
	var $grid = $('#isotope-list'),
			filters = { },
			selector = $('select.filter-sessions option:nth-child(1)').attr('data-filter-value'),
			equalizers = $('#isotope-list'),
			previousContainerHeight = $grid.height(),
			noResultsHTML = '<div class="column column-block card no-results text-center"><small><strong>No results matching search criteria.</strong><br />Try selecting another level or session from above.</small></div>',
			loaderHTML = '<div class="column column-block loader"><div data-equalizer-watch><img src="https://thomas.vanhoutte.be/miniblog/wp-content/uploads/light_blue_material_design_loading.gif" alt="..." width="100" height="100" /></div></div>';

	//console.log(defaultFilter);


	function updateStatus( event, filteredItems ) {		
		// Toggle 'no results' message
		var iso = $grid.data('isotope'),
				numItems = iso.filteredItems.length;
				//console.log('Results: '+numItems);

	  if (numItems == 0) {
	  	if ( $('.no-results').length == 0 ) {
	  		$grid.append( noResultsHTML );
	  		winWidth = $('.no-results').parent().width();
	    	itemWidth = $('.no-results').width();  
	    	itemHeight= $('.no-results').height()+62;  
	    	leftMargin = (winWidth-itemWidth)/2;  
		  	$('.no-results').css({ 'left' : '0', 'top': '0', 'marginLeft' : leftMargin });
		  	$('.no-results').animate({ 'opacity' : 1 });
		  	//console.log("displaying 'no results' message");
	  	} else {
	    	if ( $('.no-results').css('opacity') == 0 ) {
	    		$('.no-results').animate({ 'opacity' : 1 });
	    	}
	  	}
	  } else {
	  	if ( $('.no-results').length ) { 
	  		$('.no-results').remove();
	  		//console.log("removing no results msg");
	  	}
	  }

	  //console.log('prevHeight ='+previousContainerHeight );
	  //console.log('curHeight ='+$grid.height());

	  if( $grid.height() != previousContainerHeight) {
	  	//console.log('HEIGHT NEEDS ADJUSTING');

	  	newHeight = parseInt($grid.find('.column-block:visible:last').outerHeight())+parseInt($grid.find('.column-block:visible:last').css('top'));

	  	//console.log('newHeight ='+newHeight );

	  	$grid.css({'height': newHeight })
	  }

	  equalizers.on('postequalized.zf.equalizer', function() {
	  	$grid.isotope('layout');
	  });

	}

	/* Rearrange classes based on selected filters */
	$('select.filters').on( 'change', function() {
		var $this = $(this),
				group = $this.attr('data-filter-group'),
				isoFilters = [];
   	
   	previousContainerHeight = $grid.height() ;

    filters[group] = $this.find(':selected').attr('data-filter-value');
    
    for ( var prop in filters ) {
      isoFilters.push(filters[prop])
    }
    
    selector = isoFilters.join('');

	  updateClassList();
 
    return false;
	});


	function updateClassList() {

		$grid.isotope({ 
    	itemSelector : '.column-block', 
			layoutMode : 'fitRows',
			filter: selector
		});
		/* Callback after isotope re-arrangement */
		$grid.on( 'arrangeComplete', updateStatus );
	
	}

	$('select.filter-sessions').on( 'change', function() {
		$('#session-name').html( $(this).find(':selected').text() );
		$('#session-duration').html( $(this).find(':selected').data('start-date')+' to ' +$(this).find(':selected').data('end-date'));
	});

	$('select.filter-sessions').val( $('select.filter-sessions option:first').val() ).change();

	// Open Registration form in Modal box
	$('a[data-open="registration"]').on('click touch', function(e) {
		e.preventDefault();
		document.getElementById('gform_76').reset();
		$('#input_76_54').val($(this).data('class')).change();
		$('#current-session').html($(this).data('session'));
		$('#input_76_56').val( '10 weeks between ' + $('#session-duration').html() );
	})

	$('a[data-open="registration"]').keydown(function(e){
		var key = e.which;
		if(key == 13 || key == 32) {  // the Enter key code
		 $(this).click();
		}
	})


	if( getParamsFromURL('gf_paypal_return') )  {
		//$('#registration').foundation('open');
		var popup = new Foundation.Reveal($('#registration'));
		popup.open();
	}

	function getParamsFromURL(sParam) {
		var sPageURL = window.location.search.substring(1);
		var sURLVariables = sPageURL.split('&');
		for (var i = 0; i < sURLVariables.length; i++){
		  var sParameterName = sURLVariables[i].split('=');
		  if (sParameterName[0] == sParam) {
		    return sParameterName[1];
		  }
		}
	}
	
	//console.log(getParamsFromURL('gf_paypal_return'));

	//If Registration form exists
	if ( document.getElementById('gform_76') ) {

		// When Class is selected
		$('#input_76_54').on('change',function(){
			className = $('#input_76_54 option:selected').text();
			sessionName = className.substr(0, className.indexOf(' •'));

			$('#input_76_40').val(className);
			$('#input_76_45').val(sessionName);

			sessionStartDate = $('select.filter-sessions option:contains("'+sessionName+'")').data('start-date');
			sessionEndDate = $('select.filter-sessions option:contains("'+sessionName+'")').data('end-date')

			$('#input_76_56').val( '10 weeks between ' +  sessionStartDate + ' to ' + sessionEndDate);
	    
		});

		// When Package is selected
		$('#input_76_41').on('change',function(){

			tax = $('#input_76_61').val();
			value =  $(this).val();

			subamount = Number(value.substr( value.indexOf('$')+1)).toFixed(2); 
			taxamount = Number(subamount*tax).toFixed(2);
			totalamount =  Number(subamount) + Number(taxamount);
			
			/* Sub amount */
			$('label[for="input_76_49_1"]').text($('#input_76_41 option:selected').text()); //Label
			$('input[name="input_49.1"]').val($('#input_76_41 option:selected').text()); //Hidden label
			$('#input_76_49').html('$' + subamount + '');
			$('#ginput_base_price_76_49').val( subamount );

			/* Tax amount */
			$('#input_76_50').html('$' + taxamount + '');
			$('#ginput_base_price_76_50').val( taxamount );

			/* Total amount */
			$('#field_76_47 .ginput_total').html('$' + totalamount.toFixed(2) + '');
			$('#input_76_47').val( totalamount );
			$('#input_76_44').val( '$'+(totalamount.toFixed(2)) );

		});

	}

	// Make all links in Spotlight section to open in new tab
  $('#spotlight a').attr('target','_blank');

});