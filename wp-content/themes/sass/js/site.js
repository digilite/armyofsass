$(document).ready(function(){

	$(document).foundation();

  $('.slick').slick({
    infinite: true,
    autoplay: true,
  	autoplaySpeed: 12000,
  	fade: true,
  });
	
	if($(window).width() <= 768 ) {
		offsetTop = -20;
	} else {
		offsetTop = -50;
	}

	$('.local-scroll').localScroll({
		'offset' : { 'top' : offsetTop-35 },
		'hash' : true
	});

	$('#team .back').on('click', function(e) {
		$(this).find('a.button').get(0).click();
	});

});