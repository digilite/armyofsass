<?php
session_start();

global $wp_query;
$currentAuth = $wp_query->get_queried_object(); // Author/location object
$locationSlug = $currentAuth->user_nicename;
$userID = 'user_'.$currentAuth->ID; //important for ACF author ID
$locationName = get_field('location_name', $userID);
$user_info = get_userdata($currentAuth->ID);
$user_email = $user_info->user_email;
$country = get_field('country', $userID);

if ( $country == 'US' ) {
	$currency = 'USD';
} else {
	$currency = 'CAD';
}

$registration_methods = get_field('payment_methods',$userID);

$registration_start_date = get_field('registration_start_date','option');
$registration_end_date = get_field('registration_end_date','option');

$today = date('U');

get_header();

//TODO:Location user lock down

$respect_global_lock_down = get_field('respect_global_lock_down');
$loc_lock_enable = get_field('lock_location_pages_enable');
$lock_all_enable = get_field('lock_all_enable','option');

if($respect_global_lock_down == false) {
	$lock_all_enable = false;
	$lock_end_date =get_field('lock_edate');
}

if ($lock_all_enable && $loc_lock_enable == false) {
	$lock_start_date = get_field('lock_start_date','option');
	$lock_end_date = get_field('lock_end_date','option');
	$password = get_field('password','option');

	if( isset($_POST['submit_pass']) && $_POST['pass'] ) {
		$pass=$_POST['pass'];

		if( $pass== $password ) {
			$_SESSION['aospass'.$registration_start_date.'saved']= $password;
		} else {
			$error="Please try again.";
		}
	}
} else if ($lock_all_enable==false && $loc_lock_enable) {
	$loc_lock_start_date = get_field('lock_sdate');
	$loc_lock_end_date = get_field('lock_edate');
	$loc_password = get_field('password_location');
	if( isset($_POST['submit_pass']) && $_POST['pass']) {
		$pass=$_POST['pass'];

		if( $pass== $loc_password ) {
			$_SESSION['aospass'.$registration_start_date.'saved']= $loc_password;
		} else {
			$error="Please try again.";
		}
	}
} else if ($lock_all_enable && $loc_lock_enable) {

	$loc_lock_start_date = get_field('lock_sdate');
	$loc_lock_end_date = get_field('lock_edate');
	$loc_password = get_field('password_location');
	$lock_end_date =get_field('lock_edate');


	if( isset($_POST['submit_pass']) && $_POST['pass']) {
		$pass=$_POST['pass'];

		if( $pass== $loc_password ) {
			$_SESSION['aospass'.$registration_start_date.'saved']= $loc_password;
		} else {
			$error="Please try again.";
		}
	}
}

// $open_class =
// $class_id = get_field('class_id','option');
// $pass_for_class = get_field('lock_all_enable','option');

									//TODO Class pass checkbo
									$curSessions = get_field('session',  2506168 );
$class_form = 0;
$class_IDD = 0;
									if( isset($_POST['submit_pass_class']) && $_POST['passClass'] && $_POST['class_id']) {
										$passC=$_POST['passClass'];
											$cId = $_POST['class_id'];
											$admin_class_pass =  get_field('class_password',$cId);
											// pre2($cId);
											// pre2($passC);
											// pre2($admin_class_pass);

									//ne na za password
									// $admin_class_pass =  get_field('class_password',$classID);
										if( $passC== $admin_class_pass ) {
											$class_form = 1;
											$class_IDD = $cId;

											wp_localize_script('form-populate-items', 'myvars', array(
													'test01' => $class_IDD
												)
											);
//
//Fall 2018 • SUN 12:00 AM - 7:00 AM • AOS Program • Open Level • Licensee A

											// pre2($class_IDD);
											// pre2($admin_class_pass);
											// pre2($passC);
											// $open_registration = 1;
										} else {
											// $open_registration = 0;
											$error="Please try again.";
										}
									}

									//end

?>

<div class="location-wrapper">

	<?php
			if (
			 	(($lock_all_enable == false)  ||
			 	($lock_all_enable == true && ( $today < $lock_start_date || $today > $lock_end_date) ) ||
			 	($lock_all_enable == true && ( $today >= $lock_start_date && $today <= $lock_end_date ) && $_SESSION['aospass'.$registration_start_date.'saved'] == $password )) &&

				(($loc_lock_enable == false)  ||
				($loc_lock_enable == true && ( $today < $loc_lock_start_date || $today > $loc_lock_end_date) ) ||
				($loc_lock_enable == true && ( $today >= $loc_lock_start_date && $today <= $loc_lock_end_date ) && $_SESSION['aospass'.$registration_start_date.'saved'] == $loc_password ))

			) {
			?>

	<!-- Banner slider -->
	<?php if( have_rows('banners', 'option') ): ?>

	<div class="slider">
		<div class="slick">

			<?php
					while( have_rows('banners', 'option') ):
						the_row();
						$image = get_sub_field('image');
						$cta = get_sub_field('cta');

						if (get_sub_field('button_theme') == 1 ) {
							$button_theme = 'light';
							$blurb_theme = 'dark';
						} else {
							$button_theme = 'dark';
							$blurb_theme = 'light';
						}
					?>

			<div class="">
				<div class="banner" style=" background-image: url('<?php echo $image['url']; ?>');">
					<div class="content-wrapper">
						<div class="content">
							<p class="blurb <?php echo $blurb_theme; ?>"><?php echo get_sub_field('small_heading'); ?>
							</p>
							<?php if ($cta['value'] != '0' ) { ?>
							<a class="button <?php echo $button_theme; ?>"
								href="<?php echo $cta['value']; ?>"><?php echo $cta['label']; ?></a>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
			<?php endwhile; ?>

		</div>

		<div class="mask-container">
			<div class="mask bow-white-black"></div>
		</div>
	</div>
	<?php endif; ?>


	<!-- About -->
	<div class="section dark" id="about" name="about">
		<div class="row">
			<div class="small-12 columns text-center">
				<h1 class="thin caps"><?php _e('Hello ','sass');?> <strong><?php echo $locationName; ?></strong> !</h1>
			</div>
		</div>

		<?php if( have_rows('about', 'option') ): ?>
		<div class="row">
			<?php
					$i = 0;
					while( have_rows('about', 'option') ):
						$i++;
						the_row();
						$heading = get_sub_field('heading');
						$content = get_sub_field('content');

						if ($i == 1) {
							$color = "yellow";
						} else if ($i == 2) {
							$color = "green";
						} else {
							$color = "cyan";
						}
					?>
			<div class="small-12 medium-4 columns content">
				<div class="border <?php echo $color; ?>"></div>
				<h3 class="<?php echo $color; ?>"><?php echo $heading; ?></h3>
				<?php echo $content; ?>
			</div>
			<?php endwhile; ?>
		</div>
		<?php endif; ?>
	</div>
	<div class="bow-black-magenta"></div>


	<!-- Classes -->
	<div class="section light" id="classes">
		<div class="row">
			<div class="small-12 columns text-center">
				<h1 class="caps less-bottom">• <?php _e('Classes','sass');?> •</h1>
			</div>
		</div>

		<div class="row collapse">
			<div class="small-12 columns bg silver">

				<div class="small-12 large-9 large-centered xlarge-7 columns">

					<div class="row collapse" data-equalizer>
						<div class="small-12 medium-5 columns" data-equalizer-watch>

							<select class="filters filter-levels" data-filter-group="levels">
								<option value="*" data-filter-value="">All Classes &amp; Levels</option>
								<option value=".promo" data-filter-value=".promo">Free Promo Classes</option>
								<?php
										$terms = get_terms( array(
									    'taxonomy' => 'level',
									    'hide_empty' => false,
										));

										foreach ($terms as $term) {
											echo '<option value=".'.$term->slug.'" data-filter-value=".'.$term->slug.'">'.$term->name.'</option>';
										}
										?>
							</select>
						</div>
						<div class="small-12 medium-2 columns text-center">
							<div class="relative" data-equalizer-watch>
								<div class="vcentered">in</div>
							</div>
						</div>
						<div class="small-12 medium-5 columns" data-equalizer-watch>
							<select class="filters filter-sessions" data-filter-group="sessions">
								<?php
										$sessions = array();

										// Pull out all Sessions that have not ended
										$args = array(
											'post_type' => 'session',
											'author' => $currentAuth->ID,
									    'post_status' => 'publish',
									    'posts_per_page' => 3,
									    'orderby' => 'date',
									    'order' => 'DESC',
									    'meta_query' => array(
						            array(
					                'key' => 'end_date', // Check 'end_date'
					                'value' => date("Ymd"), // Compare to today
					                'compare' => '>=',
					                'type' => 'DATE'
						            )
									  	)
										);
										$sessionArray = new WP_Query( $args );

										$i = 0;
										if ( $sessionArray->have_posts() ) :
											while ( $sessionArray->have_posts() ) :
												$sessionArray->the_post();

												// For each session, check if session has any classes
												$classes = get_posts(array(
													'post_type' => 'class',
													'author' => $currentAuth->ID,
													'meta_query' => array(
														array(
															'key' => 'session',
															'value' => $post->ID,
															'compare' => 'LIKE'
														)
													),
													'posts_per_page'	=> 1,
												));

												// If session has classes, then display it as an option in the Dropdown list of Sessions
												if( $classes ):
													$sessions[] = $post->ID;

													$i++;
													// Set default current Session to the first one on the list
													if ($i == 1) {
														$currentSession = $post->ID;
													}


													$tempSessionStartDate = date('F j, Y', strtotime(get_field( 'start_date', $post->ID)));
													$tempSessionEndDate =  date('F j, Y', strtotime(get_field( 'end_date', $post->ID)));

													echo '<option value=".'.$post->ID.'" data-filter-value=".'.$post->post_name.'" data-start-date="'.$tempSessionStartDate.'" data-end-date="'.$tempSessionEndDate.'">'.ucfirst(strtolower(get_the_title($post->ID))).'</option>';
												endif;
											endwhile;
										endif; ?>
							</select>
						</div>
					</div>

				</div>
			</div>
		</div>

		<br />

		<div class="row">
			<div class="small-12 medium-10 medium-centered large-8 xlarge-6 columns text-center">
				<?php
							// Display currently shown session info
						 	$sessionName = get_the_title($currentSession);
							$sessionStartDate = date('F j, Y', strtotime(get_field( 'start_date', $currentSession)));
							$sessionEndDate =  date('F j, Y', strtotime(get_field( 'end_date', $currentSession)));

							if ($currentSession) {
						?>
				<h3><span id="session-name"><?php echo $sessionName; ?></span> Session<br />
					<span class="subheader">10 weeks between <span
							id="session-duration"><?php echo $sessionStartDate.' and '. $sessionEndDate; ?></span></span>
				</h3>
				<span class="hidden" id=""></span>
				<?php } ?>
			</div>
		</div>
		<br />


		<!-- Class results -->
		<div class="row collapse v-spacer grid-classes">
			<div class="small-12 columns">
				<?php if ( !$sessions  ) { ?>
				<div class="text-center"> <small><strong>There are no classes available.</strong></small></div>
				<?php	} else { ?>

				<div class="row collapse small-up-1 medium-up-3 large-up-4 xlarge-up-5" id="isotope-list"
					data-equalizer>
					<?php
							$meta_query = array('relation' => 'OR');
							foreach ($sessions as $value) {
						    $meta_query[] = array(
					        'key'       => 'session',
					        'value'     => $value,
					        'compare'   => 'LIKE',
						    );
							}

							$args = array(
								'post_type' 	=> 'class',
								'author' => $currentAuth->ID,
						    'post_status' => 'publish',
						    'meta_query'     => array(
					        'relation' => 'AND',
					        $meta_query,
					        'day_of_the_week' => array(
					            'key' => 'day_of_the_week',
					            'compare' => 'EXISTS'
					        ),
					        'start_time' => array(
					            'key' => 'start_time',
					            'compare' => 'EXISTS'
					        ),
					        'end_time' => array(
					            'key' => 'end_time',
					            'compare' => 'EXISTS'
					        )
					    	),
								'orderby' => array( 'session' => 'DESC' , 'day_of_the_week' => 'ASC', 'start_time' => 'ASC', 'end_time' => 'ASC' ),
								'posts_per_page'	=> -1,
							);

							$classes = new WP_Query( $args );

							if ( $classes->have_posts() ) :
								while ( $classes->have_posts() ) :
									$classes->the_post();
									$classID = get_the_ID();
									$classTerms = get_the_terms($classID, 'level');
									$dropin = 0;

									if ( $classTerms ) :
										foreach ($classTerms as $classTerm) {
											if ( strpos($classTerm->slug, 'dropin') !== false ) {
												$dropin = 1;
											}
										}
									endif;

									if ( get_field('show_in_registration_form', $classID) ) {
										$open_registration = 1;
										//TODO Class Cap
										$admin_class_pass =  get_field('class_password',$classID);
										update_field('class_id', $classID, $classID);
										//$countCap = (int) get_field('class_cap',$classID);
										//$numberCap = (int) get_field('n_students',$classID);
										//if ($numberCap == $countCap) {
										//	$open_registration = 0;
									//		update_field('show_in_registration_form', false, $classID);
									//}

									} else {
										$open_registration = 0;
									}

									

									$levels = get_the_terms( $classID, 'level' );
									if(!empty($levels)){
									  foreach($levels as $level){
									    $class_slug = $level->slug;
									    $class_name = $level->name;
											$class_title = get_the_title($post->ID);
									    $more_info = $level->description;
									  }
									}

									$sessions = get_field('session', $classID);
									if( $sessions ):
										$session_slug = '';
								    foreach( $sessions as $session):
											$session_slug .= ' '.$session->post_name;
											$session_name = $session->post_title;
											$session_ID = $session->ID;
											$session_start_date = date('U', strtotime(get_field('start_date',$session->ID)));
         							$session_end_date = date('U', strtotime(get_field('end_date',$session->ID)));

								    endforeach;
								    //wp_reset_postdata();
									endif;

									if($open_registration == 1 && ( ($today >= $registration_start_date) && ($today <= $registration_end_date) ) ) {
										$disabled = '';
									} else {
										$disabled = ' disabled';
									}
								?>

					<div class="column column-block card <?php echo $class_slug.$session_slug; if( $dropin == 1 && have_rows('free_promo_classes', $classID) ) { echo ' promo';} echo $disabled; ?>"
						data-equalizer-watch>
						<div class="front">

							<?php
										if($more_info) {
										?>
							<a class="has-tip left hint" data-tooltip aria-haspopup="true" data-disable-hover="false"
								tabindex="2" title="<?php echo $more_info; ?>"><span>?</span></a>

							<?php
										}

										$day = get_field('day_of_the_week', $classID);

										echo '<h3><small>'.strtoupper(date('D', strtotime($day['label'])).' • '.get_field('start_time', $classID).' - '.get_field('end_time', $classID)).'</small><br />'.str_replace('•','<span class="thin subheader">', $class_name).'<small>'.$class_title.'</small></span></h3>';

										echo '<h5>';
										$teachers = get_field('teacher', $classID);
										if( $teachers ):
											$i = 0;
									    foreach( $teachers as $teacher):
									    	$i++;
									    	if ($i==1) {
									    		echo get_the_title($teacher->ID);
									    	} else {
									    		echo ' & '.get_the_title($teacher->ID);
									    	}

									    endforeach;
									    //wp_reset_postdata();
										endif;

										$studios = get_field('studio', $classID);
										if( $studios ):
									    foreach( $studios as $post):
									    	setup_postdata($post);
									    	$address = get_field('address', $post->ID);
								        echo '<br /><span class="subheader"><a class="address-link" target="_blank" title="View map" rel="nofollow" href="http://maps.google.com/?q='.$address['address'].'">'.get_the_title($post->ID).'</a></span></h5>';
									    endforeach;
									    wp_reset_postdata();
										endif;

										if( have_rows('free_promo_classes', $classID ) ):
											echo '<h6 class="highlight">FREE PROMO CLASS:<br /><span class="subheader">';
									    while ( have_rows('free_promo_classes', $classID) ) : the_row();
								        echo '<br />'.the_sub_field('date');
									    endwhile;
									    echo '</span></h6><br /><br />';
										endif;
										

										if( $class_slug != "special-class" && $open_registration == 1  && ( ($today >= $registration_start_date) && ($today <= $registration_end_date) )) {
											// $admin_class_pass =  get_field('class_password');
											// pre2(empty($admin_class_pass));

											if (empty($admin_class_pass)){
												// echo '<a class="button secondary" data-class="'.$classID.'" data-session="'.$session_name.'" data-open="registration">Register</a>';
												$class_form = 1;
											} else {
												// $class_form = 0;
												if($class_IDD == $classID) {
													$class_form = 1;
													// echo '<a class="button secondary" data-class="'.$classID.'" data-session="'.$session_name.'" data-open="registration">Register</a>';
												} else {
													$class_form = 0;
												}
											}
											// pre2("tttt");
											if ($class_form == 0){
												//TODO localStorage.setItem('myCat', 'Tom');
											?>
												<form method="post" action="" id="login_form">
													<label for="password">Enter password to open registration</label>
													<input type="password" name="passClass" placeholder="*******">
													<input type="hidden" id="class_id" name="class_id" value="<?php echo $classID;?>">
													<input type="submit" name="submit_pass_class" class="button" value="Submit">
													<p>
														<font style="color:red;"><?php echo $error;?></font>
													</p>
												</form>
											<?php
											} else {
												if($locationName==="Abbotsford") {
													echo '<a class="button secondary formButtonAOS" id ="Course'.$counterTest.'" data-id="'.$classID.'" data-class="'.$classID.'" data-session="'.$session_name.'" data-open="registration">Register</a>';
												}
												else {
													$external = get_field('external_link', $userID);
													echo '<a class="button secondary formButtonAOS" id ="Course'.$counterTest.'" href="'.$external.'" target="_blank">Register</a>';
												}										
											}
											//echo '<h6>'. get_field('n_students',$classID) . '/' . get_field('class_cap',$classID) .' spots filled</h6>';

										} else {

											$note = get_field('note',$classID);
											$more_note = get_field('more_note', $classID);



											if ( $note == array() || $note == null ||  $note['label'] == ' ' ||  $note['label'] == '' ) {

											} else {

												echo '<h6>'.$note['label'];
												echo '<br /><span class="subheader">'.$more_note['label'].'&nbsp;</h6>';
											}
										}

										?>

						</div>

					</div>

					<?php endwhile; wp_reset_postdata(); ?>

					<?php else: ?>
					<div class="column column-block card no-results text-center"><small><strong>No results matching
								search criteria.</strong><br />Try selecting another level or session from
							above.</small></div>
					<?php endif;?>



				</div>

				<?php } //If sessions is not an empty array
					 ?>
			</div>
		</div>
	</div>


	<!-- Rates -->
	<div class="triangle-rise-black"></div>
	<div class="section dark" id="rates">
		<div class="row">
			<div class="small-12 columns text-center">
				<h1 class="thin caps"><strong>• Rates •</strong></h1>
			</div>
		</div>

		<?php if( have_rows('pricing', 'option') ): ?>
		<div class="row">
			<?php
					$i = 0;
					while( have_rows('pricing', 'option') ):
						$i++;
						the_row();
						$title = get_sub_field('title');
						$value = get_sub_field('value');
						$start_date = get_sub_field('start_date');
						$end_date = get_sub_field('end_date');
						$taxable = get_sub_field('taxable');

						if ($i == 1) {
							$color = "yellow";
							$full_registration_end_date = $end_date;
						} else if ($i == 2) {
							$color = "green";
						} else if ($i == 3)  {
							$color = "cyan";
						} else if ($i == 4)  {
							$color = "blue";
						}

						if ($i != 5) {
					?>
			<div class="small-12 medium-6 large-3 columns">
				<div class="border <?php echo $color; ?>"></div>
				<h3 class="<?php echo $color; ?>"><?php echo $title; ?></h3>
				<?php
		  			if ($start_date && $end_date) {
		  				echo 'Available '.date('F j',$start_date).' - '.date('F j',$end_date);
		  			} else {
		  				echo get_sub_field('note');
		  			}
		  			?>
				<br />
				<h2 class="thin price">
					<strong>$<?php echo $value; ?></strong>
					<?php if ( get_field('tax_percentage',$userID) != 0 && $taxable == 1 ) {
		  					echo ' <small>+tax</small>';
		  				} ?>
				</h2>
				<br />
				<?php echo get_sub_field('include'); ?>
			</div>
			<?php } endwhile; ?>
		</div>
		<?php endif; ?>
	</div>
	<div class="bow-black-orange"></div>


	<!-- Shows -->
	<div class="section light" id="shows">
		<div class="row">
			<div class="small-12 columns text-center">
				<h1 class="caps less-bottom">• <?php _e('Shows','sass'); ?> •</h1>
			</div>
		</div>

		<?php
				$shows = get_posts(array(
					'post_type' 	=> 'show',
					'author' => $currentAuth->ID,
			    'post_status' => 'publish',
					'order' => 'ASC',
					'posts_per_page'	=> -1,
				));
				?>
		<div class="row">
			<div class="small-12 medium-11 medium-centered large-10 xlarge-7 columns text-center">
				<?php if( $shows ):
						foreach( $shows as $show ):
							if (has_post_thumbnail($show->ID)) {
								echo '<div class="large card text-left">';
								echo get_the_post_thumbnail($show->ID);

								echo '<h3 class="less-bottom text-center">'.get_the_title($show->ID).'</h3>';

								$description_content = get_post($show->ID);
								$description = $description_content->post_content;

								$description = apply_filters('the_content', $description);
								$description = str_replace(']]>', ']]&gt;', $description);
								echo $description;

								echo '</div>';

								$shows = get_field('showtime', $show->ID);
								if( have_rows('showtime', $show->ID) ):

									$show_count = count($shows);

									if ($show_count == 1) {
										$cols = 'small-up-1';
									} elseif ($show_count == 2) {
										$cols = 'small-up-1 medium-up-2';
									} elseif ($show_count == 3) {
										$cols = 'small-up-1 medium-up-3';
									} elseif ($show_count == 4) {
										$cols = 'small-up-1 medium-up-2 large-up-3';
									} elseif ($show_count == 5) {
										$cols = 'small-up-1 medium-up-3 large-up-3';
									} elseif ($show_count == 6) {
										$cols = 'small-up-1 medium-up-3 large-up-3';
									}
									?>
				<div class="row collapse <?php echo $cols; ?> text-left" data-equalizer data-equalize-by-row="true">

					<?php while( have_rows('showtime', $show->ID) ):
										the_row();

										$door = get_sub_field('door');
										$showtime = get_sub_field('show');
										$showdate = get_sub_field('date');
										$age = get_sub_field('age');
										$note = get_sub_field('ticket_notes');
										$link = get_sub_field('ticket_link');
										$venues = get_sub_field('venue');

										echo '<div class="column column-block"><div class="card"><div data-equalizer-watch>';
										echo '<span class="subheader"><small>'.__('Doors ','sass').$door.' • '.__('Show ','sass').$showtime.'</small></span><br />';
										echo '<h3 class="thin"><strong>'.$showdate.'</strong><br />';

										if( have_rows('ticket_price') ):
											while( have_rows('ticket_price')): the_row();
									    	if(get_sub_field('enable_multiple_pricing') == 1) {
									    		$ticket = '$'.get_sub_field('in_advance').' in advance • $'.get_sub_field('at_door').' at the door';
									    	} else {
									    		$ticket = '$'.get_sub_field('general').' general admission';
									    	}
											endwhile;
										endif;

										if( $venues ):
									    foreach( $venues as $post):
									    	setup_postdata($post);
									    	$address = get_field('address', $post->ID);
												echo '<a class="small address-link" target="_blank" title="View map" rel="nofollow" href="http://maps.google.com/?q='.$address['address'].'">'.get_the_title($post->ID).'</a></h3>';
											endforeach;
											wp_reset_postdata();
										endif;

										if ($age) {
											echo '<small class="subheader">'.ucfirst($age).'<br /><span class="black">'.$ticket.'</span></small><br />';
										}

										if ($note) {
											echo '<br /><p class="subheader"><small>'.$note.'</small></p>';
										}

										echo '</div>';

										if ($link) {
											echo '<br /><a class="button secondary" href="'.$link.'" target="_blank">'.__('Buy tickets','sass').'</a><br />';
										} else {
											echo '<br /><span class="local-scroll"><a class="button disabled" href="#contact-form">'.__('Contact us for tickets','sass').'</a><br />';
										}
										echo '</div></div>';

									endwhile;
								else:
									echo '<div class="card">Showtimes and ticket info will be available soon.</div>';
								endif;
							}
						endforeach; wp_reset_postdata();  ?>
					<?php else: ?>
					<p><?php _e('No show info yet.','sass'); ?></p>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>

	<!-- Spotlight -->
	<div class="triangle-rise-bluewhite"></div>
	<div class="section bg bluewhite" id="spotlight">
		<div class="row">
			<div class="small-12 columns text-center">
				<h1 class="caps less-bottom">• <?php _e('Spotlight','sass');?> •</h1>
			</div>
		</div>

		<div class="row">
			<div class="small-12 medium-11 medium-centered large-10 xlarge-7 columns text-center">
				<?php
						$spotlight = get_posts(array(
							'post_type' 	=> 'spotlight',
							'author' => $currentAuth->ID,
					    'post_status' => 'publish',
							'posts_per_page'	=> -1,
						));

						if( $spotlight ): foreach( $spotlight as $post ): ?>

				<div class="row block">
					<div class="small-12 columns">
						<div class="card bg white text-left">
							<div class="row collapse">

								<?php if (has_post_thumbnail($post->ID)) {  ?>
								<div class="small-12 medium-4 large-5 xlarge-4 columns text-center">
									<?php echo '<p>'.get_the_post_thumbnail($post->ID, 'post-thumb').'</p>'; ?>
								</div>
								<div class="small-12 medium-8 large-7 xlarge-8 columns">
									<?php } else { ?>
									<div class="small-12 columns">
										<?php } ?>
										<div class="content">
											<h2 class="thin caps"><?php echo get_the_title($spot->ID); ?></h2>
											<?php
												$post_content = get_post($post->ID);
												$content = $post_content->post_content;
												$content = apply_filters('the_content', $content);
												$content = str_replace(']]>', ']]&gt;', $content);
												echo $content;
												?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<?php endforeach; wp_reset_postdata(); ?>
					<?php else: ?>
					<p><?php _e('No posts yet.','sass'); ?></p>
					<?php endif; ?>

				</div>
			</div>
		</div>
		<div class="triangles-bluewhite-black"></div>

		<!-- Team -->
		<div class="section dark" id="team">
			<div class="row">
				<div class="small-12 columns text-center">
					<h1 class="caps">• AOS <?php echo $locationName; ?> <?php _e('Team','sass');?> •</h1>
				</div>
			</div>

			<div class="row expanded collapse" data-equalizer>
				<?php

					$hq = get_posts(array(
						'post_type' 	=> 'team',
				    'post_status' => 'publish',
						'posts_per_page'	=> -1,
						'orderby' => 'term_id',
						'order' => 'ASC',
						'tax_query' => array(
					    array(
					      'taxonomy' => 'position',
					      'field' => 'id',
					      'terms' => array(56,60)  // CEO + General Manager
					    )
					  )
					));

					if( $hq ):
						$teamSize = count($hq);

						$team_count = count_user_posts( $currentAuth->ID, 'team' )+$teamSize;
						switch ($team_count) {
					    case 1:
				        $cols = 'small-12 medium-6 medium-centered large-5 xlarge-4';
				        break;
				      case 2:
				        $cols = 'small-12 medium-6';
				        break;
					    case 3:
				        $cols = 'small-12 medium-6 large-4';
				        break;
					    default:
				        $cols = 'small-12 medium-6 large-4';
				        break;
						}


						foreach( $hq as $post ): setup_postdata( $post );
						$hq_name = get_the_title();
						$hq_photo = get_the_post_thumbnail_url($post->ID, 'team-thumb');
						$hq_photo_full = get_the_post_thumbnail_url($post->ID, 'full');
						$hq_bio = apply_filters('the_content', $post->post_content);
						$position = wp_get_post_terms($post->ID, 'position', array("fields" => "all"));
						$hq_position = $position[0]->name;
						$hq_slug = $post->post_name;
					?>

				<div class="<?php echo $cols; ?> columns card">
					<div class="front" style="background-image: url('<?php echo $hq_photo; ?>');" data-equalizer-watch>
						<div class="info">
							<?php
								echo '<h2>'.$hq_name.'</h2><br />';
								echo '<h3>'.$hq_position.'</h3><br /><h4>ARMY of SASS™</h4>';
								?>
						</div>
					</div>

					<div class="back">
						<a class="button hollow" data-open="bio-<?php echo $hq_slug; ?>">View profile</a>
					</div>

					<div class="reveal full" id="bio-<?php echo $hq_slug; ?>" data-reveal data-animation-in="fade-in"
						and data-animation-out="fade-out">
						<div class="row expanded bg black">
							<div class="small-10 small-centered medium-12 columns">
								<h3 class="thin text-center">
									<strong><?php echo $hq_name;?></strong><br /><?php echo $hq_position; ?></h3>
							</div>
						</div>

						<div class="button-container">
							<button class="close-button" data-close aria-label="Close modal" type="button"><span
									aria-hidden="true">&times;</span></button>
						</div>

						<div class="section">
							<div class="row">
								<div class="small-12 medium-centered large-9 xlarge-7 columns">
									<img src="<?php echo $hq_photo_full; ?>" class="avatar"
										alt="<?php echo $hq_name;?>" /><br />
									<?php echo $hq_bio; ?>
								</div>
							</div>
							<p>&nbsp;</p>
							<p>&nbsp;</p>
						</div>
					</div>

				</div>

				<?php
					endforeach; wp_reset_postdata(); endif;

					$terms = get_terms(array(
						'taxonomy' => 'position',
						'orderby' => 'term_id',
						'exclude' => array(56,60) // CEO + General Manager
						)
					);

					$i = 0;

					foreach($terms as $term):

						$team = get_posts(
							array(
								'post_type' 	=> 'team',
								'author' => $currentAuth->ID,
						    'post_status' => 'publish',
						    'tax_query' => array(
		              array(
		                'taxonomy' => 'position',
		                'field' => 'slug',
		                'terms' => $term->slug
		              )
		            ),
								'posts_per_page'	=> -1,
								'order' => 'ASC'
			        )
						);

						if( $team ):

							$teamSize += count($team);

							foreach( $team as $post ): setup_postdata( $post );
								$i++;

					?>

				<div class="<?php echo $cols; ?> columns card end">
					<?php
						$photo = get_the_post_thumbnail_url($post->ID, 'team-thumb');
						$photo_full = get_the_post_thumbnail_url($post->ID, 'full');

						if (!$photo) {
						 $photo = get_template_directory_uri().'/img/avatar.gif';
						}

						?>
					<div class="front" style="background-image: url('<?php echo $photo; ?>');" data-equalizer-watch>
						<div class="info">
							<?php
								echo '<h2>'.get_the_title().'</h2><br />';
								$position = wp_get_post_terms($post->ID, 'position', array("fields" => "all"));
								echo '<h3>'.$position[0]->name.'</h3>';
								?>
						</div>
					</div>
					<div class="back">
						<a class="button hollow" data-open="bio-<?php echo $post->post_name; ?>">View profile</a>
					</div>
					<div class="reveal full" id="bio-<?php echo $post->post_name; ?>" data-reveal
						data-animation-in="fade-in" and data-animation-out="fade-out">
						<div class="row expanded bg black">
							<div class="small-10 small-centered medium-12 columns">
								<h3 class="thin text-center">
									<strong><?php echo get_the_title();?></strong><br /><?php echo $position[0]->name; ?>
								</h3>
							</div>
						</div>

						<div class="button-container">
							<button class="close-button" data-close aria-label="Close modal" type="button"><span
									aria-hidden="true">&times;</span></button>
						</div>

						<div class="row section">
							<div class="small-12 medium-centered large-9 xlarge-7 columns">
								<?php if ($photo_full) { ?><img src="<?php echo $photo_full; ?>" class="avatar"
									alt="<?php echo get_the_title();?>" /><br /><?php } ?>
								<?php if (!empty_content($post->post_content)) { the_content(); } else { echo '<p class="text-center">Profile is coming soon.</p>'; } ?>
							</div>
						</div>
						<p>&nbsp;</p>
						<p>&nbsp;</p>
					</div>
				</div>

				<?php endforeach; wp_reset_postdata();
					endif;
				endforeach;
					/*
					echo '<h1>'.$teamSize.'</h1>';
					echo '<h1>'.$i.'</h1>';
					*/
				?>
			</div>
		</div>
		<div class="bow-black-cyan"></div>


		<!-- Contact -->
		<?php
			if( have_rows('faqs', 'option') ):
			?>
		<div class="section light" id="contact">
			<div class="row">
				<div class="small-12 columns text-center">
					<h1 class="caps less-bottom">• <?php _e('FAQ','sass');?> •</h1>
					<h4 class="thin">Check out these frequently asked questions, or <span class="local-scroll"><a
								class="text-link" href="#contact-form"><strong>contact us</strong></a></span> using the
						form below.</h4>
				</div>
			</div>

			<div class="row">
				<div class="small-12 medium-10 medium-centered large-8 xlarge-6 columns text-center">

					<ul class="accordion text-left" data-accordion data-allow-all-closed="true">

						<?php
							while( have_rows('faqs', 'option') ):
								$i++;
								the_row();
								$question = get_sub_field('question');
								$answer = get_sub_field('answer');
							?>

						<li class="accordion-item" data-accordion-item>
							<a href="#" class="accordion-title">
								<h3 class="caps"><?php echo $question; ?></h3>
							</a>
							<div class="accordion-content" data-tab-content>
								<p><?php echo $answer; ?></p>
							</div>
						</li>

						<?php endwhile; ?>

						<li class="accordion-item is-active" data-accordion-item>
							<a href="#" class="accordion-title" name="contact-form">
								<h3 class="caps">Questions or comments ? Contact us here !</h3>
							</a>
							<div class="accordion-content" data-tab-content>
								<?php echo do_shortcode('[gravityforms id="77" title="false" description="false" ajax="true" field_values="location='.$locationName.'&locationemail='.$user_email.'&locationid='.$userID.'"]'); ?>
							</div>
						</li>
					</ul>

				</div>
			</div>
		</div>
		<?php endif; ?>

	</div>

	<div class="reveal full" id="registration" data-reveal data-animation-in="fade-in" and
		data-animation-out="fade-out">
		<div class="row expanded bg black">
			<div class="small-12 columns">
				<h3 class="thin text-center"><?php echo $locationName;?> <span
						id="current-session"></span><br /><strong>REGISTRATION</strong></h3>
			</div>
		</div>

		<div class="button-container">
			<button class="close-button" aria-label="Close modal" type="button"><span
					aria-hidden="true">&times;</span></button>
		</div>

		<div class="section">
			<div class="row">
				<div class="small-12 medium-centered large-9 xlarge-7 columns">
					<?php if ($registration_methods[0] == 'External') {

			  			if(  ($today >= $registration_start_date) && ($today <= $registration_end_date)  ) {
					  		echo do_shortcode('[gravityforms id="82" title="false" description="false" ajax="true" tabindex="100" field_values="location='.$locationName.'"]');
					  	} else {
					  		echo '<div class="text-center">';
				  			echo '<p>Registration will be open on '.date('F j, Y g:i a T',$registration_start_date).'</p>';
				  			echo '</div>';
				  		}

				  		/*
			  			echo '<div class="text-center">';
			  			echo '<p>This location has an external registration system. Follow the link below to go to there.</p>';
			  			echo '<a class="button primary large caps" href="'.get_field('external_link',$userID).'" target="_blank">Register now</a>';
			  			echo '</div>';
			  			*/
			  		} elseif ( empty($registration_methods[0]) ) {
	            echo '<div class="text-center">';
			  			echo '<p>This location is not taking online registration at the moment.</p>';
			  			echo '</div>';
	          } else {
	          	if(  ($today >= $registration_start_date) && ($today <= $registration_end_date)  ) {
					  		echo do_shortcode('[gravityforms id="76" title="false" description="false" ajax="true" tabindex="100" field_values="location='.$locationName.'&locationemail='.$user_email.'&currency='.$currency.'&custompaypal='.get_field( 'paypal_email', $userID ).'&tax='.get_field( 'tax_percentage', $userID ).'&locationid='.$userID.'&duration=10 weeks between '.$sessionStartDate.' and '.$sessionEndDate.'"]');
					  	} else {
					  		echo '<div class="text-center">';
				  			echo '<p>Registration will be open on '.date('F j, Y g:i a T',$registration_start_date).'</p>';
				  			echo '</div>';
				  		}

					  } ?>
				</div>
			</div>
			<p>&nbsp;</p>
		</div>


		<?php } else { ?>


		<div class="section light text-center">
			<div class="row">
				<div class="small-12 medium-8 medium-centered large-6 xlarge-5 columns text-center">
					<form method="post" action="" id="login_form">
						<h1 class="caps">Hello <?php echo $locationName; ?> dancers!</h1>
						<h3>Early registration is currently private.</h3>
						<p>If you have a password, enter it below to proceed. Otherwise, public early registration will
							open on <?php echo date(' l F jS, Y',strtotime('+1 day',$lock_end_date));?>.</p>
						<p>We look forward to dancing with you!</p>
						<input type="password" name="pass" placeholder="*******">
						<input type="submit" name="submit_pass" value="Continue to <?php echo $locationName; ?>"
							class="button">
						<p>
							<font style="color:red;"><?php echo $error;?></font>
						</p>
					</form>
				</div>
			</div>
		</div>

		<?php } ?>
	</div>

	<?php get_footer(); ?>