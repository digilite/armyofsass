<?php
get_header(); ?>
	
	<div class="bow-black-magenta"></div>

	<div id="primary" class="row expanded">
		<main id="main" class="small-12 columns" role="main">

			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'page' );

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

	<!-- Etiquette -->
	<div class="triangle-rise-black"></div>
	<div class="row expanded collapse" id="etiquette">
		<div class="small-12 columns">
			<?php 
			$post = get_post('1154'); 
			$content = apply_filters('the_content', $post->post_content); 
			echo $content;  
			?>
		</div>
	</div>
	<div class="bow-black-cyan"></div>

	<!-- FAQ -->
	<?php 
	if( have_rows('faqs', 'option') ):
	?>
	<div class="section light" id="faq">
		<div class="row">
  		<div class="small-12 columns text-center">
  			<h1 class="caps less-bottom">• <?php _e('FAQ','sass');?> •</h1>
  			<h4 class="thin">Check out these frequently asked questions, or <span class="local-scroll"><a class="text-link" href="#contact-form"><strong>contact us</strong></a></span> using the form below.</h4>
  		</div>
  	</div>

  	<div class="row">
  		<div class="small-12 medium-10 medium-centered large-8 xlarge-6 columns text-center">
				
				<ul class="accordion text-left" data-accordion data-allow-all-closed="true">
		  		
		  		<?php 
					while( have_rows('faqs', 'option') ): 
						$i++;
						the_row(); 
						$question = get_sub_field('question'); 
						$answer = get_sub_field('answer');
					?>

				  <li class="accordion-item" data-accordion-item>
				    <a href="#" class="accordion-title"><h3 class="caps"><?php echo $question; ?></h3></a>
				    <div class="accordion-content" data-tab-content>
				      <p><?php echo $answer; ?></p>
				    </div>
				  </li>
				 
				 	<?php endwhile; ?>

				 	<li class="accordion-item is-active" data-accordion-item>
				    <a href="#" class="accordion-title" name="contact-form"><h3 class="caps">Questions or comments ? Contact us here !</h3></a>
				    <div class="accordion-content" data-tab-content>
				      <?php echo do_shortcode('[gravityforms id="77" title="false" description="false" ajax="true"]'); ?>
				    </div>
				  </li>
				</ul>

  		</div>
  	</div>
  </div>
	<?php endif; ?>
	
	<!-- TEAM -->
	<div class="bow-green-black"></div>
	<div class="section light" id="team">
		<div class="row">
  		<div class="small-12 columns text-center">
  			<h1 class="caps">• <?php _e('AOS Team','sass');?> •</h1>
  		</div>
  	</div>

  	<div class="row expanded collapse" data-equalizer>
			<?php 

			$cols = 'small-12 medium-6 large-4';	
			
			$hq = get_posts(array(
				'post_type' 	=> 'team',
		    'post_status' => 'publish',
				'posts_per_page'	=> -1,
				'orderby' => 'term_id',
				'order' => 'ASC',
				'tax_query' => array(
			    array(
			      'taxonomy' => 'position',
			      'field' => 'id',
			      'terms' => array(56,60)
			    )
			  )
			));

			if( $hq ): 
				$teamSize = count($hq);

				foreach( $hq as $post ): setup_postdata( $post ); 
				$hq_name = get_the_title(); 
				$hq_photo = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'team-thumb')[0];
				$hq_photo_full = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'team-full')[0];
				$hq_bio = apply_filters('the_content', $post->post_content); 
				$position = wp_get_post_terms($post->ID, 'position', array("fields" => "all"));
				$hq_position = $position[0]->name;
				$hq_slug = $post->post_name;
			?>

			<div class="<?php echo $cols; ?> columns card">
				<div class="front" style="background-image: url('<?php echo $hq_photo; ?>');" data-equalizer-watch>
					<div class="info">
						<?php
						echo '<h2>'.$hq_name.'</h2><br />';
						echo '<h3>'.$hq_position.'</h3><br /><h4>ARMY of SASS™</h4>';
						?>
					</div>
				</div>

				<div class="back">
					<a class="button hollow" data-open="bio-<?php echo $hq_slug; ?>">View profile</a>
				</div>

				<div class="reveal full" id="bio-<?php echo $hq_slug; ?>" data-reveal data-animation-in="fade-in" and data-animation-out="fade-out">
					<div class="row expanded bg black">
						<div class="small-10 small-centered medium-12 columns">
						  <h3 class="thin text-center"><strong><?php echo $hq_name;?></strong><br /><?php echo $hq_position; ?></h3>
						</div>
					</div>

					<div class="button-container">
						<button class="close-button" data-close aria-label="Close modal" type="button"><span aria-hidden="true">&times;</span></button>
					</div>

				  <div class="section">
				  	<div class="row">
					  	<div class="small-12 medium-centered large-9 xlarge-7 columns">
					  		<?php if ($hq_photo_full) { ?><img src="<?php echo $hq_photo_full; ?>" class="avatar" alt="<?php echo $hq_name;?>" /><br /><?php } ?>
					  		<?php echo $hq_bio; ?>
					  	</div>
					  </div>
					  <p>&nbsp;</p>
					  <p>&nbsp;</p>
				  </div>
				</div>

			</div>

			<?php
			endforeach; wp_reset_postdata(); endif;

			$terms = get_terms(array(
				'taxonomy' => 'position',
				'orderby' => 'term_id',
				'order' => 'ASC',
				'exclude' => array(56,60) // CEO & Manager
				)
			);

			$i = 0;

			foreach($terms as $term):

				$team = get_posts(
					array(
						'post_type' 	=> 'team',
						'author__not_in' => array('1','2','16','25','1043'), //Admin, lannie, aos, etobicoke, aos-us
				    'post_status' => 'publish',
				    'tax_query' => array(
              array(
                'taxonomy' => 'position',
                'field' => 'slug',
                'terms' => $term->slug
              )
            ),
						'posts_per_page'	=> -1,
						'order' => 'ASC',
						'orderby' => 'title'
	        )
				);
				
				if( $team ): 
					$teamSize += count($team);
					 
					foreach( $team as $post ): setup_postdata( $post ); 
						$i++; 
			?>

			<div class="<?php echo $cols; ?> columns card <?php if($i == $teamSize-2) { echo 'end'; } ?>">
				<?php 

				$photo = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'team-thumb')[0];
				$photo_full = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'team-full')[0];

				if (!$photo) {
				 $photo = get_template_directory_uri().'/img/avatar.gif';
				}

				?>
				<div class="front" style="background-image: url('<?php echo $photo; ?>');" data-equalizer-watch>
					<div class="info">
						<?php
						echo '<h2>'.get_the_title().'</h2><br />';
						$position = wp_get_post_terms($post->ID, 'position', array("fields" => "all"));
						$locationID = $post->post_author;
						echo '<h3>'.$position[0]->name.'</h3><br />';
						echo '<h4>'.get_field('location_name','user_'.$locationID).' • '.get_field('province_state','user_'.$locationID).' • <span class="caps">'.get_field('country','user_'.$locationID).'</span></h4>';
						?>
					</div>
				</div>
				<div class="back">
					<a class="button hollow" data-open="bio-<?php echo $post->post_name; ?>">View profile</a>
				</div>
				<div class="reveal full" id="bio-<?php echo $post->post_name; ?>" data-reveal data-animation-in="fade-in" and data-animation-out="fade-out">
					<div class="row expanded bg black">
						<div class="small-10 small-centered medium-12 columns">
						  <h3 class="thin text-center"><strong><?php echo get_the_title();?></strong><br /><?php echo $position[0]->name; ?></h3>
						</div>
					</div>

					<div class="button-container">
						<button class="close-button" data-close aria-label="Close modal" type="button"><span aria-hidden="true">&times;</span></button>
					</div>

				  <div class="row section">
				  	<div class="small-12 medium-centered large-9 xlarge-7 columns">
				  		<?php if ($photo_full) { ?><img src="<?php echo $photo_full; ?>" class="avatar" alt="<?php echo get_the_title();?>" /><br /><?php } ?>
				  		<?php if (!empty_content($post->post_content)) { the_content(); } else { echo '<p class="text-center">Profile is coming soon.</p>'; } ?>	
				  	</div>
				  </div>
				  <p>&nbsp;</p>
				  <p>&nbsp;</p>
				</div>
			</div>

			<?php endforeach; wp_reset_postdata(); 
			endif;
		endforeach;

					/* 
					echo '<h1>'.$teamSize.'</h1>';
					echo '<h1>'.$i.'</h1>';
					*/
		?>
  	</div>
  </div>

<?php
get_footer();
