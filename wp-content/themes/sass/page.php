<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sass
 */

get_header(); ?>
	
	<div class="bow-black-magenta"></div>

	<div id="primary" class="row">
		<main id="main" class="small-12 medium-11 medium-centered large-10 xlarge-7 columns" role="main">

			<div class="section">
				<?php while ( have_posts() ) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<header class="entry-header">
							<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
						</header><!-- .entry-header -->

						<div class="entry-content">
							<?php
								if ( has_post_thumbnail() ) { ?>
								<div class="section even text-center">
								<?php
							    the_post_thumbnail('full'); ?>
							  </div>
							  <?php
								}

								the_content();
							?>
						</div><!-- .entry-content -->

						
					</article><!-- #post-## -->

				<?php endwhile; ?>

			</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
