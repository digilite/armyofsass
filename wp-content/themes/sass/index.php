<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sass
 */

get_header(); ?>
	

	<div class="bow-black-magenta"></div>

	<div id="primary" class="row expanded collapse">
		<main id="main" class="small-12 columns" role="main">

			<div class="section light">
				<h1 class="entry-title text-center">For AOS community</h1>

				<?php if ( have_posts() ) : ?>
				<div class="row expanded collapse small-up-1 medium-up-2 large-up-3 xlarge-up-4 blog-grid">
				<?php while ( have_posts() ) : the_post(); ?>

					<div class="column column-block">
						<div class="card">
							<div class="front" style="background-image:url('<?php echo get_the_post_thumbnail_url($post->ID, 'full'); ?>');">
							</div>
							<div class="back" onclick="javascript:location.href='<?php echo get_permalink(); ?>';">
								<div class="vcentered">
									<a href="<?php echo get_permalink(); ?>" class="button hollow light title"><?php echo get_the_title(); ?></a>
								</div>
							</div>
						</div>
					</div>			
				<?php	endwhile; ?>
				</div>

				<div class="row">
					<div class="small-12 columns text-center">
						<div class="pagination">
							<?php
							$args = array(
								'prev_text'          => __('«'),
								'next_text'          => __('»'),
							);

							echo paginate_links($args); ?>
						</div>
					</div>
				</div>

				<?php 
				else :
					echo '<p class="text-center">No posts yet.</p>';
				endif; 
				?>
			</div>
			
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
