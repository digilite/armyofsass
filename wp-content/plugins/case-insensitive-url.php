<?php
/*
Plugin Name: Disable Case-Sensitive URL
Description: Custom plugin to make all Wordpress URLs case-insensitive ('Toronto' is the same as 'toronto')
Version: 1.0.0
Author: Pixel Dreams
Author URI: http://pixeldreams.com/
*/

function case_insensitive_url() {
    if (preg_match('/[A-Z]/', $_SERVER['REQUEST_URI'])) {
        $_SERVER['REQUEST_URI'] = strtolower($_SERVER['REQUEST_URI']);
        $_SERVER['PATH_INFO']   = strtolower($_SERVER['PATH_INFO']);
    }
}
if(!is_admin()){
    add_action('init', 'case_insensitive_url');
}
?>