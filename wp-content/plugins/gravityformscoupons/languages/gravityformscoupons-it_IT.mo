��    :      �  O   �      �     �           	           &     -     ;     I     U     b     n     }     �     �     �     �     �  4   �  A     @   _  2   �  J   �          &     +     0     =     Z     g     w  .   �     �  [   �  .        L     ^  A   l  
   �  [   �  [   	  ,   q	  U   �	     �	  (   
  "   6
     Y
     q
     w
     �
  �   �
  (     &   9  '   `     �     �     �     �  Q  �     -     5     F     a     i     p     �     �     �     �     �      �  )         *     3     H     X  5   u  ?   �  =   �  E   )  c   o     �     �     �     �     �          +     =  2   M     �  _   �  /   �  "        B  @   V     �  ^   �  U     :   [  k   �       1     '   N     v     �     �     �  �   �  ,   E  )   r  -   �     �     �  
             8   %   7   (   3           $   4   "                     :       5                                    #   )                *                                &           '   6                         /   
                 2   	   1   !                      +   9   ,   0   .   -        Amount Any Form Applies to Which Form? Apply Coupon Coupon Amount Coupon Basics Coupon Code Coupon Code: Coupon Name Coupon Options Coupon code: %s is invalid. Coupon fields are not editable Edit Edit this feed End Date Enter coupon name. Enter the amount to be deducted from the form total. Enter the date when the coupon should expire. Format: YYYY-MM-DD. Enter the date when the coupon should start. Format: YYYY-MM-DD. Enter the number of times coupon code can be used. Enter the value users should enter to apply this coupon to the form total. Expires Flat Form Gravity Form Gravity Forms Coupons Add-On Invalid Form Invalid coupon. Is Stackable Only one Coupon field can be added to the form Percentage(%) Please enter a valid Coupon Code. The Coupon Code can only contain alphanumeric characters. Please enter a valid date. Format: YYYY-MM-DD. Reset Usage Count Select a Form Select the Gravity Form you would like to integrate with Coupons. Start Date The %s has been successfully uninstalled. It can be re-activated from the %splugins page%s. The Coupon Code entered is already in use. Please enter a unique Coupon Code and try again. This coupon can't be applied more than once. This coupon can't be used in conjunction with other coupons you have already entered. This coupon has expired. This coupon has reached its usage limit. This coupon is currently inactive. This field is required. Title Usage Count Usage Limit When the "Is Stackable" option is selected, this coupon code will be allowed to be used in conjunction with another coupon code. You must add a Product field to the form You must add a Total field to the form You must enter a value for coupon code. https://www.gravityforms.com https://www.rocketgenius.com please wait rocketgenius Project-Id-Version: Gravity Forms Coupons Add-On 2.9
Report-Msgid-Bugs-To: https://gravityforms.com/support
Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>
Language-Team: Italian (Italy) (https://www.transifex.com/rocketgenius/teams/30985/it_IT/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
POT-Creation-Date: 2019-08-14T20:17:14+00:00
PO-Revision-Date: 2019-05-11 07:53+0000
Language: it_IT
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Zanata 4.6.2
X-Poedit-Basepath: ../
X-Poedit-Bookmarks: 
X-Poedit-Country: United States
X-Poedit-KeywordsList: __;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;esc_html_x:1,2c;
X-Poedit-SearchPath-0: .
X-Poedit-SourceCharset: utf-8
X-Textdomain-Support: yes
 Importo Qualsiasi modulo Si applica a quale modulo? Applica Coupon Importo del coupon Elementi di base del coupon Codice coupon Codice coupon: Nome del coupon Opzioni del coupon Codice coupon: %s non è valido. I campi del coupon non sono modificabili. Modifica Modifica questo feed Data della fine Inserisci il nome del coupon Inserisci l'importo da dedurre dal totale del modulo. Inserisci la data di scadenza del coupon. 
Formato: AAAA-MM-GG. Inserisci la data di inizio del coupon. 
Formato: AAAA-MM-GG. Inserisci il numero di volte in cui il coupon può essere utilizzato. Inserisci i valori che gli utenti devono inserire per applicare questo coupon al totale del modulo. Scade Fisso Modulo Gravity Form Add-On Coupons di Gravity Forms Il modulo non è valido Coupon non valido È accumulabile Solo un campo coupon può essere aggiunto al form. Percentuale(%) Inserisci un codice coupon valido. Il codice coupon può contenere solo caratteri alfanumerici. Inserisci una data valida. Formato: AAAA-MM-GG. Reimposta il conteggio di utilizzo Seleziona un modulo Seleziona il modulo Gravity Form che vuoi integrare con Coupons. Data di inizio Il %s è stato disinstallato con successo. Può essere riattivato dalla %spagina dei plugin%s. Il Codice coupon inserito è già in uso. Inserisci un Codice coupon unico e riprova. Questo coupon non può essere applicato più di una volta. Questo coupon non può essere utilizzato in concomitanza con gli altri coupon che sono stati già inseriti. Questo coupon è scaduto. Questo coupon ha raggiunto il limite di utilizzo. Questo coupon al momento non è attivo. Questo campo è obbligatorio. Titolo Conteggio di utilizzo Limite di utilizzo Quando l'opzione "È Accumulabile" è selezionata, questo codice coupon potrà essere utilizzato assieme ad altri codici coupon. Devi aggiungere un campo Prodotto al modulo. Devi aggiungere un campo Totale al modulo Devi inserire un valore per il codice coupon: https://www.gravityforms.com https://www.rocketgenius.com Attendi... rocketgenius 