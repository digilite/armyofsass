�S[<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:4208;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2017-08-10 15:41:53";s:13:"post_date_gmt";s:19:"2017-08-10 19:41:53";s:12:"post_content";s:3400:"<div class="page" title="Page 2">
<div class="layoutArea">
<div class="column">
<h2 style="text-align: center;">HEELS DANCE 101</h2>
<h2 style="text-align: center;"><em>A new program by the creator of Army of Sass</em></h2>
<h3 style="text-align: center;">Ease your way into the commercial dance industry by learning how to dance in HEELS!</h3>
<p style="text-align: center;"><em><a href="http://armyofsass.com/classes/">Looking for Army of Sass Classes? Click here!</a></em></p>
ABOUT HEELS DANCE 101~

You want to dance for Beyoncé? Be in music videos? Tour with international celebrity artists? Travel the world on cruise ships? Perform in Vegas or on Broadway?...

You’re going to have to learn how to do EVERYTHING in HEELS!

HEELS DANCE 101 is dance class/workshop that provides young dancers proper training in heels dancing, led in a safe environment by licensed instructors.

Created by <a href="https://armyofsass.com/carla-%20catherwood/" target="_blank" rel="noopener">Carla Catherwood</a> (Founder/CEO of ARMY of SASS dance), HEELS DANCE 101 promises to provide a dance experience to help young dancers take their current jazz, street, funk, and hip hop technique and teach them how to do it all in HEELS. After years as a professional dancer, choreographer, and after casting many projects, it was clear to Carla just how important it is for dancers, especially young women, to learn how to elevate their studio training and be able to execute advanced choreography in heels!
<blockquote>“At the age of 20 I was a hired as a professional dancer on a cruise ship contract. We spent 5 weeks learning our choreography in jazz shoes and sneakers. At the end of the rehearsal contract we were given a pair of 3 inch heels and told to ‘just do it all in these now!’ I was like... ummm ok... I almost busted my ankle a few times! Let’s just say it wasn’t the safest way to learn how to dance in heels! Between turns, kicks, progressions, partner work, being thrown around, and learning to land safely in the shoes I was given, I was pretty much just winging it. I don’t think it’s safe and that is why HEELS DANCE 101 has been created- to provide a training environment for dancers considering a long-term career as a professional dancer”</blockquote>
<blockquote>“As a choreographer for corporate/film/television projects, I’ve witnessed this all too many times: dancers come in, nail the choreography while you teach it to them, and when you ask them to put on HEELS, they crumble... and end up being cut in the first round. Not because of their ability to dance, but because of their inability to dance in HEELS. Building a solid heels technique will truly help you succeed!”</blockquote>
– CARLA CATHERWOOD Founder/CEO of HEELS DANCE 101 &amp; ARMY of SASS DANCE
<p style="text-align: center;"><iframe src="https://www.youtube.com/embed/VH2aWFGRb6c" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe></p>
&nbsp;
<h2>Heels Dance 101 curriculum includes:</h2>
<ul>
 	<li>Ankle strengthening exercises</li>
 	<li>Weight placement in heels for jazz</li>
 	<li>Weight placement in heels for hip hop/street funk</li>
 	<li>Turns/progression/jumping in heels</li>
 	<li>Footwear recommendations</li>
</ul>
<h3 style="text-align: center;">CLICK BELOW to find a teacher near you!</h3>
</div>
</div>
</div>";s:10:"post_title";s:15:"Heels Dance 101";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:13:"heelsdance101";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2018-03-06 09:47:57";s:17:"post_modified_gmt";s:19:"2018-03-06 14:47:57";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:35:"http://armyofsass.com/?page_id=4208";s:10:"menu_order";i:0;s:9:"post_type";s:4:"page";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}