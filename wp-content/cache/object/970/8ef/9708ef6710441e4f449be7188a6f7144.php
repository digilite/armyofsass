�S[<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:7:"2005147";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2017-11-14 10:46:51";s:13:"post_date_gmt";s:19:"2017-11-14 15:46:51";s:12:"post_content";s:4266:"<h1 style="text-align: center;"><em>ARMY of SASS PRESENTS</em></h1>
<h1 style="text-align: center;">"LEARN TO BE A PUSSYCAT DOLL"</h1>
<h1 style="text-align: center;">with ROBIN ANTIN</h1>
<h2 style="text-align: center;">(Creator + Founder of PCD)</h2>
<h2 style="text-align: center;">Saturday, January 20th</h2>
<h2 style="text-align: center;">11:30am-2:30pm</h2>
<p style="text-align: center;">The Underground Dance Studio (STUDIO: GRANDE)</p>
<p style="text-align: center;"><a href="https://maps.google.com/?q=220+Richmond+St.+W+-+Toronto,+ON&amp;entry=gmail&amp;source=g">220 Richmond St. W - Toronto, ON</a></p>
&nbsp;
<p style="text-align: center;">Join us for a 3 hour workshop with Robin Antin, Creator + Founder of the Pussycat Dolls, that consists of:</p>
<p style="text-align: center;">* PCD FIT (warm up + conditioning)</p>
<p style="text-align: center;">* PCD Barre, PCD Chair</p>
<p style="text-align: center;">* PCD Choreography</p>
<p style="text-align: center;">* Intimate Q + A session!</p>
<p style="text-align: center;">Only 30 spots available!</p>
<p style="text-align: center;">$200.00 + HST</p>

<h3 style="text-align: center;"><strong><a href="https://form.jotform.com/73044169454256">CLICK HERE TO REGISTER</a></strong></h3>
&nbsp;

<strong>ABOUT ROBIN ANTIN</strong>

Originally from Los Angeles, Robin Antin has made a name for herself as a choreographer, producer, director, designer and entrepreneur. In 1995, Robin Antin created, “The Pussycat Dolls” originally as a live club show at Johnny Depp’s infamous Viper Room in Hollywood. After 8 years of putting her show on every weekend at the viper room she decided to take the show to the infamous Roxy, on sunset Blvd performing to sold out crowds with faces in the audiences including Cameron Diaz. Soon after Robin signed a deal at Interscope records where they took the club show and turned into an international super group, recognized worldwide as one of the most successful girl groups of all time with over 20 million albums sold.

Robin then decided to brand The PUSSYCAT dolls by dominating the Las Vegas strip. Signing a deal with Caesars Palace to create her own “Pussycat Dolls Lounge and Pussycat Dolls Casino” located next to PURE nightclub.

Soon after she landed two seasons of her own reality show on CW, “<em>Search for the Next Doll</em>”. In 2007, during season two, Robin created a new girl group, “Girlicious” who made a Platinum self-titled album and went on to sell out arenas across the world.

Robins Pussycat dolls brand went into a collection of lingerie, called “Shhhh, by Robin Antin,” as well as two of her own fitness DVDs titled “Pussycat Dolls Workout, by Robin Antin.” She has worked with many artists along the way including Christina Applegate, Gwen Stefani, Christina Aguilera, Fergie, Charlize Theron and Scarlett Johansson. Robin also worked with Cameron Diaz, Drew Barrymore and Lucy Liu when they played “Undercover Pussycat Dolls” in “<strong>Charlie’s Angels Full Throttle</strong>”.

Robin has choreographed for many hit TV shows including “<em>American Idol</em>”, The Emmys® with host Jimmy Fallon, Jimmy then asked Robin to come back on the <em>Tonight Show</em> choreographing a Dirty Dancing Spoof for him and Drew Barrymore, the 2011 opening at “The Billboard Awards” with host Ken Jeong and “<em>The Academy Awards</em>”. Most recently, Robin choreographed the only musical number at the 2014 <em>Emmys</em>®, featuring Weird Al Yankovic and Andy Samberg. She also served as a recurring guest judge on “<em>So You Think You Can Dance</em>” and was a permanent judge on the Lifetime show “Abby’s Ultimate Dance Competition”.

Robin then created group "GRL," in which they featured on Pitbull’s single "Wild Wild Love." GRL's self-titled EP and their single "Ugly Heart", produced by Dr. Luke and Max Martin, jumped to #1 on the Australian charts.

Robin recently has opened her own dance studio with partner Kenny Wormald, called Playground LA located in Melrose Ave.

Many more exciting things to come for Robin … stay tuned!

Follow Robin on Instagram: <a href="https://www.instagram.com/robinantin/">@RobinAntin</a>

&nbsp;";s:10:"post_title";s:40:"Pussycat Dolls Workshop with Robin Antin";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:11:"pcdworkshop";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2018-01-02 12:08:23";s:17:"post_modified_gmt";s:19:"2018-01-02 17:08:23";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:32:"http://armyofsass.com/?p=2005147";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}