�S[<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:4:"2112";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2016-11-14 15:04:22";s:13:"post_date_gmt";s:19:"2016-11-14 20:04:22";s:12:"post_content";s:4884:"<h1 style="text-align: center;">2017 National Fundraiser</h1>
<h1 style="text-align: center;">#NGDLB- No Good Dancer Left Behind</h1>
<h2 style="text-align: center;"><em>Army of Sass Partnership Campaign with Soul Dancers Charity</em></h2>
<h2 style="text-align: center;"><span style="color: #ff0000;"><a style="color: #ff0000;" href="http://tilt.tc/xwkE?s=wb&amp;u=jlong6468" target="_blank" rel="noopener noreferrer">DONATE HERE!</a></span></h2>
&nbsp;

<strong><u>NOURISH. INSPIRE. EMPOWER.</u></strong>

It’s heart wrenching, real and yes it does happen...

There are communities where women and children are forced to skip education to either sell food, or even their bodies, on the street just to survive.

Meet our sponsored dance leaders: Julie and Juan Sebastian, from Medellin, Columbia. Born and raised within the infamous “Pablo Escobar” region, these individuals have completely immersed their lives in dance when they barely had the means to do so.

This national fundraising initiative will help bring them from Columbia to Canada to further their education at a national dance teacher conference. Having never travelled outside their own country, this opportunity will be life-changing . It will also provide them boundary-breaking educational experiences to take back to their communities to help inspire the children they work with that truly need it!
<h1 style="text-align: center;"><span style="color: #ff0000;">Our fundraising goal: $7,500</span></h1>
WHAT FUNDS RAISED WILL PAY FOR:

*VISA APPLICATIONS

* FLIGHTS

* HOTEL

* FOOD AND BEVERAGE

* NATIONAL DANCE CONFERENCE

An <a href="https://www.tilt.com/tilts/no-good-dancer-left-behind">online giving campaign</a> will be live from November 15-May 30 on <a href="https://www.tilt.com/tilts/no-good-dancer-left-behind">Tilt.com</a>; AOS locations are encouraged to host events, share info, collect donations, etc to help support this initiative. <strong> </strong>

&nbsp;

<strong><u>ABOUT JULIE, ANA ELISA, &amp; JUAN SEBASTIAN</u></strong>

<em> <u>Julie’s Story: </u></em>

Born and raised in Medellin, Julie has had to overcome a lot of obstacles. Julie is passionate on educating, helping, and providing a brighter future for her own community. Julie is currently studying at the University of Antioquia, majoring in bachelor of dance, in addition to teaching at a local orphanage and at Ana Elisa Dance School. Growing up in a rougher neighborhood, it has to be arranged by the orphanage as to what time she teaches so she can arrive home safely. This young girl is at the tipping point of her professional dance career and a trip like this can truly lead her in a positive direction!

&nbsp;

<em><u>Juan Sebastian’s Story:</u></em>

Juan Sebastian is the director of the Soul Dancers charity in Columbia.  He also teaches English at one of the orphanages the charity supports. Without Sebastian, Soul Dancers would never have been able to get up and running. He is always working within the community closely and was the one who discovered Ana Elisa and Julie. His compassionate and trustworthy nature makes him the perfect administrator to distribute the money required to make the charity work and happen. He provides all the monthly financial and expense reports and disburses payment to the orphanages, dance teachers, and so forth. He makes sure that all those that are directly involved in providing dance &amp; English lessons receive their fees.

Juan Sebastian will be coming out to learn more about the business of dance and programing, as well as act as translator for Julie.

<em><u>
ABOUT SOUL DANCERS CHARITY </u></em>

<em><img class="alignleft wp-image-2118 size-medium" src="http://armyofsass.com/wp-content/uploads/2016/11/Soul-Dancers_final-logo-300x181.png" alt="soul-dancers_final-logo" width="300" height="181" />SOUL DANCERS is a charitable dance movement to nourish the body, inspire the mind, and empower the souls of impoverished women and children in third world countries.</em>

<em>Our mission is to provide an afterschool program for impoverished children consisting of a nourishing meal, 30 minutes of basic English language training, and 1 hour of fun, instructional dance. We want to share the gift of creative dance while bringing joy and hope to the most vulnerable children of the world.</em>

<em>We aim to employ vulnerable women to run the programs thus providing life skills to enable independence and self-sustenance.</em>

&nbsp;
<p style="text-align: center;"><strong>YOU’RE NOT JUST IMPACTING 2 LIVES. THIS ACT OF KINDNESS WILL KEEP ON GIVING TO HUNDREDS OF CHILDREN FOR YEARS TO COME!</strong></p>
&nbsp;

Soul Dancers: <a href="http://www.souldancers.org/">http://www.souldancers.org/</a>

Army of Sass: <a href="http://armyofsass.com/">http://armyofsass.com/</a>

&nbsp;";s:10:"post_title";s:26:"No Good Dancer Left Behind";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:22:"nogooddancerleftbehind";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2017-05-23 14:56:15";s:17:"post_modified_gmt";s:19:"2017-05-23 18:56:15";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:29:"http://armyofsass.com/?p=2112";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}