�S[<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:7;s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2012-05-08 15:54:08";s:13:"post_date_gmt";s:19:"2012-05-08 15:54:08";s:12:"post_content";s:3775:"<div class="row about collapse">
<div class="small-12 medium-10 medium-centered large-8 xlarge-6 columns">
<h2 class="thin caps text-center"><strong>Army of Sass&#x2122;</strong>
where confidence, community and dance come together</h2>
<span class="highlight army-dark white text-center" style="padding: 30px;">ARMY of SASS&#x2122; is a training and performance program for people from all dancing levels to build confidence, gain physical strength while developing dance skills in heels.</span>

</div>
</div>
<div class="section light">
<h1 class="text-center thin caps less-bottom"><small>•••••</small>
<strong>WE WANT YOU</strong>
as our sassy new recruit!</h1>
<img src="https://armyofsass.com/wp-content/uploads/2018/05/Army-of-sass-heels-dance.jpg" alt="ARMY of SASS" width="100%" />

&nbsp;
<div class="row about collapse">
<div class="small-12 medium-10 medium-centered large-8 xlarge-6 columns">
<h2 class="thin caps text-center">Train. Sweat. Perform.</h2>
There are 3 different 10-week sessions per year: Fall, Winter, and Spring. Every week you will train for 2 hours on drills and choreography. At the end of the session, you will showcase your new sassy moves in a themed performance with your fellow AOS dancers! If you're not able to commit to the full AOS program you're still welcome to experience our DROP-IN class and learn all of the moves! Just check out our <a href="../classes/">locations</a>, levels, dates, and times in your area!

&nbsp;
<h2 class="thin caps text-center">All skill levels</h2>
<div class="row collapse text-center">
<div class="small-12 medium-4 columns">
<div class="card">

<strong>LEVEL 1 • PRIVATES</strong>
Absolute Beginners to Beginner-Trained Dancers

<a class="has-tip bottom hint large" tabindex="2" title="NO DANCE EXPERIENCE NECESSARY! This level of AOS is built around the every day woman! The “PRIVATES” is a group for women who have always wanted to dance, perform, and be on stage in front of a crowd! Now is your chance ladies! In this training session, one of our “Sergeants of Sass” (AOS teachers) will teach you all of the moves from scratch so you can be sassy and confident in your big performance." data-tooltip="" data-disable-hover="false">More info</a>

</div>
</div>
<div class="small-12 medium-4 columns">
<div class="card">

<strong>LEVEL 2 • CORPORALS</strong>
Intermediate to Advanced in Heels Dance

<a class="has-tip bottom hint" tabindex="2" title="This is the perfect level in the AOS training program for women who have a trained dance background and can pick up choreography quickly, execute basic jazz and hip hop techniques, and can really dance in heels! This level is also a great goal for our “PRIVATES” to work towards! This group of ladies will train in our ‘SASS’ classes as well as learn 2 or more choreographed pieces for their term end show!" data-tooltip="" data-disable-hover="false">More info</a>

</div>
</div>
<div class="small-12 medium-4 columns">
<div class="card">

<strong>LEVEL 3 • LIEUTENANTS</strong>
Advanced to Professional in Jazz/Hip Hop/Heels Dance

<a class="has-tip bottom hint" tabindex="2" title="These women have worked for years in the studio and are ready to be challenged! With extensive dance training in hip-hop, jazz, and ballet these women are ready to show it all in their HEELS! These ladies get to work with numerous leading local choreographers to not only challenge them, but to ensure their artistic growth as dancers is being nourished. The LIEUTENANTS will perform 3 or more numbers with the opportunity to be cast as principals in our AOS term end shows." data-tooltip="" data-disable-hover="false">More info</a>

</div>
</div>
</div>
</div>
</div>
</div>";s:10:"post_title";s:5:"About";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:5:"about";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2018-05-04 11:20:51";s:17:"post_modified_gmt";s:19:"2018-05-04 15:20:51";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:32:"http://armyofsass.com/?page_id=7";s:10:"menu_order";i:0;s:9:"post_type";s:4:"page";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"2";s:6:"filter";s:3:"raw";}}