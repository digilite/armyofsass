�S[<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":23:{s:2:"ID";s:4:"4300";s:11:"post_author";s:1:"1";s:9:"post_date";s:19:"2017-08-16 12:27:16";s:13:"post_date_gmt";s:19:"2017-08-16 16:27:16";s:12:"post_content";s:1499:"<p style="text-align: center;"><strong>ARMY OF SASS is hiring!</strong></p>
We are looking for a dynamic, talented administrator for part-time work in our TORONTO, ON office.

Working closely with our CEO and GM, this person will be responsible for a variety of roles including:
<ul>
 	<li>General administrative tasks, including email communications with students and licensees</li>
 	<li>In-office support to our CEO</li>
 	<li>Assistance with marketing initiatives, including social media</li>
 	<li>Special event administration</li>
</ul>
Our ideal candidate has/is:
<ul>
 	<li>A problem-solver</li>
 	<li>A positive attitude and strong interpersonal communication</li>
 	<li>Interested in software and cloud-based technology (willingness to learn new systems)</li>
 	<li>Attention to detail</li>
 	<li>Deadline-oriented</li>
 	<li>Strong written language skills (English)</li>
</ul>
This important team member would have interest/experience in the following areas:
<ul>
 	<li>Mailchimp</li>
 	<li>Wordpress</li>
 	<li>Eventbrite</li>
 	<li>Facebook (Business)</li>
 	<li>Instagram (Business)</li>
 	<li>Microsoft Excel</li>
</ul>
&nbsp;

<strong>IMPORTANT JOB INFORMATION</strong>

Start Date: September 2017

Hours Weekly (combination of in-office and remote work): 20 hours

Wage: $15 starting wage
<h2><a href="https://www.jotform.com/build/72247124995261" target="_blank" rel="noopener">APPLY HERE</a>.</h2>
<h2>Applications close August 31, 2017.</h2>";s:10:"post_title";s:13:"We're Hiring!";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:7:"recruit";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2017-08-17 13:55:20";s:17:"post_modified_gmt";s:19:"2017-08-17 17:55:20";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";s:1:"0";s:4:"guid";s:29:"http://armyofsass.com/?p=4300";s:10:"menu_order";s:1:"0";s:9:"post_type";s:4:"post";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";}}