��          �      l      �     �  I   �     B     Y     _  �   e  
          i   '     �     �     �     �     �       +   "  
   N     Y  5   a     �  0  �     �  I   �     C     Z     `  �   f  
          i   (     �     �     �     �     �       +   #  
   O     Z  ;   b     �                    
                                                         	                     AJAX Thumbnail Rebuild AJAX Thumbnail Rebuild allows you to rebuild all thumbnails on your site. Ajax Thumbnail Rebuild Done. Error If you find this plugin useful, I'd be happy to read your comments on the %splugin homepage%s. If you experience any problems, feel free to leave a comment too. Last image No attachments found. Note: If you've changed the dimensions of your thumbnails, existing thumbnail images will not be deleted. Only rebuild featured images Reading attachments... Rebuild All Thumbnails Rebuild Thumbnails Rebuild all Thumbnails Rebuilding %s of %s (%s)... Select which thumbnails you want to rebuild Toggle all cropped https://wordpress.org/plugins/ajax-thumbnail-rebuild/ junkcoder, ristoniinemets PO-Revision-Date: 2018-09-19 16:00:56+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.4.0-alpha
Language: en_CA
Project-Id-Version: Plugins - AJAX Thumbnail Rebuild - Stable (latest release)
 AJAX Thumbnail Rebuild AJAX Thumbnail Rebuild allows you to rebuild all thumbnails on your site. Ajax Thumbnail Rebuild Done. Error If you find this plugin useful, I'd be happy to read your comments on the %splugin homepage%s. If you experience any problems, feel free to leave a comment too. Last image No attachments found. Note: If you've changed the dimensions of your thumbnails, existing thumbnail images will not be deleted. Only rebuild featured images Reading attachments... Rebuild All Thumbnails Rebuild Thumbnails Rebuild all Thumbnails Rebuilding %s of %s (%s)... Select which thumbnails you want to rebuild Toggle all cropped https://en-ca.wordpress.org/plugins/ajax-thumbnail-rebuild/ junkcoder, ristoniinemets 