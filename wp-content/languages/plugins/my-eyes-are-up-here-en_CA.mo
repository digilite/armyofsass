��          �      L      �  ?   �            P        l     z     �  6   �     �  s   �  '   [     �     �  
   �     �     �  5   �       �   %  ?   #     c     p  P   }     �     �     �  6   �     5  s   I  '   �     �     �  
             '  5   A     w                             
   	                                                                    %d %s found, thumbnails regenerated to fit them into crop area. Add hotspots Detect faces Detects faces during thumbnail cropping and moves the crop position accordingly. Edit hotspots Face detection Forget found faces Manually add hotspots that you want to avoid cropping. My Eyes Are Up Here Please note this is basic face detection and won't find everything. Use hotspots to highlight any that were missed. This plugin requires javascript to work Thumb Previews Unknown failure reason face faces hotspot hotspots http://interconnectit.com https://github.com/interconnectit/my-eyes-are-up-here interconnect/it PO-Revision-Date: 2016-04-14 23:44:48+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.1.0-alpha
Project-Id-Version: Stable (latest release)
 %d %s found, thumbnails regenerated to fit them into crop area. Add hotspots Detect faces Detects faces during thumbnail cropping and moves the crop position accordingly. Edit hotspots Face detection Forget found faces Manually add hotspots that you want to avoid cropping. My Eyes Are Up Here Please note this is basic face detection and won't find everything. Use hotspots to highlight any that were missed. This plugin requires JavaScript to work Thumb Previews Unknown failure reason face faces hotspot hotspots http://interconnectit.com https://github.com/interconnectit/my-eyes-are-up-here interconnect/it 